def print_menu():
   print('\nMenu:')
   print('1. append')
   print('2. sort')
   print('3. print')
   print('4. quit')

my_list = []
quit = False
input_line = None

while not quit:
    print_menu()

    command = int(input("\nEnter command: "))

    if command == 1:
       item = input("Item? ")
       my_list.append(item)
    elif command == 2:
       my_list.sort()
    elif command == 3:
       print(my_list)
    elif command == 4:
       quit = True

