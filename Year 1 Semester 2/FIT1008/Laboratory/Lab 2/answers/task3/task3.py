"""
@descripton: Main module which starts up  and links everything together.
This is seperate from the other modules so that the other modules
can be used for other stuff

@author: Lee Loong Kuan
@since: 28-07-2015
@modified: 05-08-2015
@param: none
@return: none

Testing menu input:
input: 1    expected: Puzzle GUI appears with solved config
            results : Puzzle GUI appears with solved config

input: 2    expected: Puzzle GUI appears with random config
            results : Puzzle GUI appears with random config

input: 3    expected: Puzzle Builder Wizard starts up
            results : Puzzle Builder Wizard starts up

input: 4    expected: Exit Program
            results : Exit Program

input: a    expected: Error: Invalid Input
            results : Error: Invalid Input

input: ''   expected: Error: Invalid Input
            results : Error: Invalid Input

input: 5    expected: Error: Invalid Input
            results : Error: Invalid Input

Testing puzzle GUI:
NOTE: empPos is the position of the empty spot in the puzzle as a tuple
input: Keyboard Presses:
           <UP>, empPos=(3,3)    expected: empPos=(2,3)    result: empPos=(2,3)
           <UP>, empPos=(0,0)    expected: empPos=(0,3)    result: empPos=(0,3)
           <DOWN>, empPos=(3,3)  expected: empPos=(3,3)    result: empPos=(3,3)
           <DOWN>, empPos=(0,0)  expected: empPos=(1,0)    result: empPos=(1,0)
           <LEFT>, empPos=(3,3)  expected: empPos=(3,2)    result: empPos=(3,2)
           <LEFT>, empPos=(0,0)  expected: empPos=(0,0)    result: empPos=(0,0)
           <RIGHT>, empPos=(3,3) expected: empPos=(3,3)    result: empPos=(3,3)
           <RIGHT>, empPos=(0,0) expected: empPos=(0,1)    result: empPos=(0,1)
           <q>    expected: nothing happens    result: nothing happpens

       GUI button presses:
           'random' button    expected: puzzle replaced with a random puzzle
                              result  : puzzle replaced with a random puzzle
           'reset' button     expected: Undo all of the user's moves
                              result  : Undo all of the user's moves
           
"""

from tkinter import *
import App_Class
import Puzzle_Random
import Enter_Puzzle

BasePuzzle = [[' 1', ' 2', ' 3', ' 4'],
              [' 5', ' 6', ' 7', ' 8'],
              [' 9', '10', '11', '12'],
              ['13', '14', '15', ' X']]


def start_app(puzzle_config):
    master = Tk()
    App_Class.App(master, puzzle_config)

    master.mainloop()


def print_menu():
    print("")
    print("1. Start from solved configuration.")
    print("2. Start from random configuration.")
    print("3. Start from user defined configuration.")
    print("4. Quit")


quit = False
while not quit:
    print_menu()
    command = input("Enter command: ")
    if command == '1':
        start_app(BasePuzzle)
        print("\033c")
    elif command == '2':
        puzz_config = Puzzle_Random.create_rand_puzz()
        start_app(puzz_config)
        print("\033c")
    elif command == '3':
        print("\033c")
        puzzle_entered_state = Enter_Puzzle.get_input()
        for i in puzzle_entered_state[1]:
            print(i)
        if puzzle_entered_state[0] == True:
            print("This puzzle is Valid!")
            start_app(puzzle_entered_state[1])
        else:
            print("This puzzle is Invalid!")
    elif command == '4':
        quit = True
    else:
        print("\033c")
        print("Error: Invalid Input")
