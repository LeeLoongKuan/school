"""
@description: Implements a class that contains the puzzle
This class contains functions in order to manipulate the puzzle easily

@author: Lee Loong Kuan
@since: 31-07-2015
@modified: 04-08-2015
@param: The puzzle as a list of lists
@return: Varies between functions
"""
from copy import deepcopy

BasePuzzle = [[' 1', ' 2', ' 3', ' 4'],
              [' 5', ' 6', ' 7', ' 8'],
              [' 9', '10', '11', '12'],
              ['13', '14', '15', ' X']]


class PuzzleClass:
    def __init__(self, in_puzz):
        self.puzzle = [[]]
        self.ori_puzz = in_puzz
        self.represent = ""
        self.empPos = (-1, -1)
        self.Reset()

    def set_puzz(self, puzzle_to_set):
        self.ori_puzz = puzzle_to_set
        self.Reset()

    def Refresh(self):
        self._create_represent()
        self.empPos = self._find_empty_pos()

    def Reset(self):
        self.puzzle = deepcopy(self.ori_puzz)  # Pass list by value
        self._create_represent()
        self.empPos = self._find_empty_pos()

    def _find_empty_pos(self):
        """Returns the current position of empty spot in the given puzzle"""
        i = 0
        while i < len(self.puzzle):
            j = 0
            while j < len(self.puzzle):
                if self.puzzle[i][j] == " X":
                    return (i, j)
                j += 1
            i += 1
        return (i, j)

    def _create_represent(self):
        """Create a representation the puzzle in a better format"""
        rep = ""
        for i in self.puzzle:
            rep += "|"
            for j in i:
                rep += str(j) + " "
            rep += "|\n"
        self.represent = rep

    def find_tile(self, tile_to_find):
        """Returns the position of a certain tile in the given puzzle"""
        tile_pos = (-1, -1)
        i = 0
        while i < len(self.puzzle):
            j = 0
            while j < len(self.puzzle):
                if self.puzzle[i][j] == tile_to_find:
                    tile_pos = (i, j)
                j += 1
            i += 1
        return tile_pos

    def swap(self, pos1, pos2):
        """Swaps the element at pos1 and pos2 of puzzle"""
        swap_tmp = self.puzzle[pos1[0]][pos1[1]]
        self.puzzle[pos1[0]][pos1[1]] = self.puzzle[pos2[0]][pos2[1]]
        self.puzzle[pos2[0]][pos2[1]] = swap_tmp
        self.Refresh()

    def print_puzzle(self):
        print(self.represent)

    def move(self, dir_to_move):
        next_empty = (self.empPos[0] + dir_to_move[0],
                      self.empPos[1] + dir_to_move[1])
        if (next_empty[0] in range(4)) and (next_empty[1] in range(4)):
            self.swap(self.empPos, next_empty)
        else:
            print("Error: Ivalid Move")
