"""
@description: A module that generates a random puzzle
Note that the puzzle generated is a valid puzzle and possible to solve

@author: Lee Loong Kuan
@since: 02-08-2015
@modified: 04-08-2015
@param: none
@return: The puzzle generated as a list of lists
"""
import Check_Valid
import random


def create_rand_puzz():
    """Return a random valid puzzle"""
    # Create a list of possible elements in the puzzle
    elements_possible = []
    for i in range(1, 16):
        if len(str(i)) == 1:
            elements_possible.append(" " + str(i))
        else:
            elements_possible.append(str(i))
    elements_possible.append(" X")

    is_puzzle_valid = False
    while not is_puzzle_valid:
        random.shuffle(elements_possible)
        puzzle_generated = Check_Valid.build_puzz_from_list(elements_possible)
        is_puzzle_valid = Check_Valid.check_possible(puzzle_generated)

    return puzzle_generated


