"""
@descripton: Provides the GUI to the puzzle.
Contains 2 buttons, "Random", and "Reset"
Allows user to move pieces in the puzzle using arrow keys.

@author: Lee Loong Kuan
@since: 28-07-2015
@modified: 04-08-2015
@param: Arrow key press
@return: Displays changes to the puzzle made by other modules
"""

from tkinter import *
import Puzzle_Class
import Puzzle_Random


BasePuzzle = [[' 1', ' 2', ' 3', ' 4'],
              [' 5', ' 6', ' 7', ' 8'],
              [' 9', '10', '11', '12'],
              ['13', '14', '15', ' X']]


class App:
    def __init__(self, master):
        self.puzzleText = StringVar()

        self.puzzleDisplay = Label(master, justify=LEFT,
                                   textvariable=self.puzzleText,
                                   font=("Courier", 16))
        self.puzzleDisplay.pack()

        self.moves = {'w': (-1, 0), 'a': (0, -1),
                      's': (1, 0), 'd': (0, 1), 'q': None}
        self.puzz = Puzzle_Class.PuzzleClass(BasePuzzle)

        self.random = Button(master, text="Randomise", command=self.Randomise)
        self.random.pack(side=LEFT)

        self.btn_reset = Button(master, text="Reset", command=self.Reset)
        self.btn_reset.pack(side=RIGHT)

        master.bind('<Up>', self.Up)
        master.bind('<Down>', self.Down)
        master.bind('<Left>', self.Left)
        master.bind('<Right>', self.Right)
        self.Refresh()

    def Refresh(self):
        self.puzzleText.set(self.puzz.represent)

    def Reset(self):
       self.puzz.set_puzz(BasePuzzle)
       self.Refresh()

    def Randomise(self):
       puzzle_temp = Puzzle_Random.create_rand_puzz() 
       self.puzz.set_puzz(puzzle_temp)
       self.Refresh()

    def Up(self, event):
        self.puzz.move((-1, 0))
        self.puzzleText.set(self.puzz.represent)

    def Down(self, event):
        self.puzz.move((1, 0))
        self.puzzleText.set(self.puzz.represent)

    def Left(self, event):
        self.puzz.move((0, -1))
        self.puzzleText.set(self.puzz.represent)

    def Right(self, event):
        self.puzz.move((0, 1))
        self.puzzleText.set(self.puzz.represent)

