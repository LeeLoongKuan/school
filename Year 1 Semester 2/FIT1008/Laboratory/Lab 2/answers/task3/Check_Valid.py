"""
@description: Module that contains functions
to assist in checking if a puzzle is valid and possible to solve.

@author: Lee Loong Kuan
@since: 01-08-2015
@modified: 04-08-2015
@param:
       check_possible: The puzzle as a list of lists
       build_puzz_from_list: The elements of a puzzle as just one list
@return:
        check_possible: True if puzzle is possible to solve, False otherwise
        build_puzz_from_list: A puzzle as a list of lists
"""
from Puzzle_Class import PuzzleClass


def check_possible(puzzle):
    """Assuming the puzzle input is correct, is it possible to solve?"""
    puzzle_class = PuzzleClass(puzzle)
    empty_pos = puzzle_class.find_tile(" X")
    city_distance = (3 - empty_pos[0]) + (3 - empty_pos[1])

    swaps = 0
    # First put empty spot in the right place
    if (city_distance > 0):
        puzzle_class.swap((3, 3), empty_pos)
        empty_pos = (3, 3)
        swaps += 1

    # Now the rest of the puzzle
    chk_pnt = [-1, -1]
    for i in range(15):
        chk_pnt[0] = i // 4
        chk_pnt[1] = i % 4
        current_tile = str(i + 1)
        if len(current_tile) == 1:
            current_tile = ' ' + current_tile
        if not puzzle_class.puzzle[chk_pnt[0]][chk_pnt[1]] == current_tile:
            swap_pos = puzzle_class.find_tile(current_tile)
            puzzle_class.swap(swap_pos, chk_pnt)
            swaps += 1

    if (city_distance + swaps) % 2 == 0:
        return True
    else:
        return False


def build_puzz_from_list(elements):
    """Build a 4X4 puzzle from a list of 16 elements"""
    puzzle_built = []
    for i in range(4):
        tmp_list = []
        for j in range(4):
            ele_list_index = (i * 4) + j
            tmp_list.append(elements[ele_list_index])
        puzzle_built.append(tmp_list)
    return puzzle_built
