"""
@description: A module to showcase the Check_Valid module.
Guides the user through steps to create their own puzzle configuration.
checks if the puzzle created by the user is possible to solve.

@author: Lee Loong Kuan
@since: 03-08-2015
@modified: 05-08-2015
@param: none
@return: prints "...Valid" if the puzzle created is possible to solve.
         prints "...Invalid" if the puzzle created is not possible to solve.
"""
import Check_Valid
import App_Class
from tkinter import *


def Print_Prompt(tile_to_insert, available_slots, puzzle_list):
    print("\033c")
    print("=====================")
    print("Puzzle Builder Wizard")
    print("=====================\n")
    print("Slots avaliable: ")
    for i in range(4):
        print("", end='|')
        for j in range(4):
            print(available_slots[(i*4) + j], end='|')
        print('', end='\n')
    position = -1
    flag = False
    while(not flag):
        try:
            position = int(input("Choose where to insert tile '" +
                                 str(tile_to_insert) + "' : "))

            if len(str(position)) == 1:
                pos_str = ' ' + str(position)
            else:
                pos_str = str(position)

            if pos_str in available_slots:
                flag = True
                available_slots[position - 1] = 'NA'
            else:
                print("Error: That slot is not available")

        except ValueError:
            print("Error: Input must be a number from open slots listed above")

    if len(str(tile_to_insert)) == 1:
        puzzle_list[position - 1] = ' ' + str(tile_to_insert)
    else:
        puzzle_list[position - 1] = str(tile_to_insert)


def get_input():
    available_slots = [' 1', ' 2', ' 3', ' 4',
                       ' 5', ' 6', ' 7', ' 8',
                       ' 9', '10', '11', '12',
                       '13', '14', '15', '16']
    puzzle_list = [0, 0, 0, 0,
                   0, 0, 0, 0,
                   0, 0, 0, 0,
                   0, 0, 0, 0]

    for i in range(1, 16):
        Print_Prompt(i, available_slots, puzzle_list)

    Print_Prompt(' X', available_slots, puzzle_list)

    final_puzzle = Check_Valid.build_puzz_from_list(puzzle_list)

    if (Check_Valid.check_possible(final_puzzle)):
        return (True, final_puzzle)
    else:
        return (False, final_puzzle)
