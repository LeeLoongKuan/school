def is_prime(n):
    k = 2

    # Initialise flag with True value
    # If n passes the following checks it will be a prime
    flag = True

    # fixed from "if (n = 2)". "==" is used for comparisons
    if (n == 2):
        # fixed from "flag = true". Boolean values are capatilised
        flag = True
    # fixed from "n % 2 == 1", we want to catch multiples of 2
    # As they are definately not primes
    # Also checks for 1 as 1 is not a prime
    elif (n % 2 == 0 or n == 1):
        # fixed from "flag = false". Boolean values are capatilised
        flag = False
    else:
        # Changed from "k*k < n" because we also need from all integres <= sqrt(n)
        while (k*k <= n):
            if (n % k == 0):
                # fixed from "flag = false". Boolean values are capatilised
                flag = False
                break
            # Added "k += 1" to check for k in range [3, sqrt(n))
            # Also fixes a bug which caused an infinite loop
            k += 1

    return flag
