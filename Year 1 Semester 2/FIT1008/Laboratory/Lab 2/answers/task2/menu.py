"""
@descripton: Menu module that allows the user to manipulate a list

@author: Lee Loong Kuan
@since: 28-07-2015
@modified: 05-08-2015
@param: user input: any character
@return: If character input is between 1 - 10,
         one of the functions defined is executed
         Else, print 'Error: Invalid Input'

Testing:
input: 1
       list = []
       input: 1    expected: ['1']    result: ['1']
       input: a    expected: ['a']    result: ['a']

input: 2
       list=['1', '3', '2']    expected: ['1', '2', '3']    result: ['1', '2', '3']
       list=['d', 'a', '1']    expected: ['1', 'a', 'd']    result: ['1', 'a', 'd']
       list=[]                 expected: []                 result: []

input: 3
       list=[]           expected: prints "[]"        result: prints "[]"
       list=['a', '']    expected: prints "['a', '']" result: prints "['a', '']"

input: 4
       list=['1']    expected: []    result: []
       list=[]       expected: []    result: []

input: 5
       list=['1', '2']    expected: ['2', '1']    result: ['2', '1']
       list=[]            expected: []            result: []

input: 6
       list=['1','2']     expected: list=['1'], returns '2'    result: list=['1'], prints '2'
       list=[]            expected: nothing happens            result: nothing happens

input: 7
       list=['1','2']    expected: 2     result: 2
       list=[]           expected: 0     result: 0

input: 8
       list=[]    input:'a', 0     expected: ['a']    result: ['a']
       list=[]    input:'a', 1     expected: Error    result: Error
       list=[]    input:'a', 'a'   expected: Error    result: Error

input: 9
       list=['1', '2', '1']    input: '1'    expected: 0    result: 0
       list=['1', '2', '1']    input: 'a'    expected: False    result: False          
       list=[]    input: ''    expected: False    result: False

input: 10
       expected: Exit program    result: Exit program 

input: a    expected: Error: Invalid command Input
            results : Error: Invalid command Input

input: ''   expected: Error: Invalid command Input
            results : Error: Invalid command Input

input: 11   expected: Error: Invalid command Input
            results : Error: Invalid command Input
"""
def print_menu():
    print('\nMenu:')
    print('1. append')
    print('2. sort')
    print('3. print')
    print('4. clear')
    print('5. reverse')
    print('6. pop')
    print('7. size')
    print('8. insert')
    print('9. find')
    print('10. quit')

my_list = []
quit = False
input_line = None

while not quit:
    print_menu()

    command = input("\nEnter command: ")

    if command == '1':
        item = input("Item? ")
        my_list.append(item)
    elif command == '2':
        my_list.sort()
    elif command == '3':
        print(my_list)
    elif command == '4':
        my_list = []
    elif command == '5':
        reverse_list = []
        for i in my_list:
            reverse_list.insert(0, i)
        my_list = reverse_list
    elif command == '6':
        if len(my_list) > 0:
            print(my_list[-1])
            del(my_list[-1])
    elif command == '7':
        print(len(my_list))
    # insert: can put negative? put position much greater then length?
    elif command == '8':
        item = input("Item? ")
        position = input("Position in list? ")
        if position.isdigit():
            if int(position) in range(len(my_list) + 1):
                my_list.insert(int(position), item)
            else:
                print("Error: Position given out of bounds of the list")
        else:
            print("Error: Position have to be a positive number")
    elif command == '9':
        item = input("Item to find? ")
        flag = False
        for i in range(len(my_list)):
            if item == my_list[i]:
                print(i)
                flag = True
                break
        if not flag:
            print("False")
    elif command == '10':
        quit = True
    else:
        print("Error: Invalid command input")
