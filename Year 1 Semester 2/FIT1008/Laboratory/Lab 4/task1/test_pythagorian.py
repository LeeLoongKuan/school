"""
    @author Brendon Taylor
    @since 20/07/2015
    @modified 20/07/2015
    @modified 08/08/2015 Moved test cases into separate files
    @modified 12/08/2015 Added debug messages
"""

from pythagorian import create_triple
from random import randint
import unittest

def testTriple(triple):
    return triple[0]*triple[0] + triple[1]*triple[1] == triple[2]*triple[2]

class TestPythagorian(unittest.TestCase):
    def testCreateTriple(self):
        MAX_VALUE = 1000
        MAX_TEST_CASES = 100

        # Test m,n = 0
        self.assertTrue(testTriple(create_triple(0, 0)))

        # Test random values
        for i in range(MAX_TEST_CASES):
            m = randint(-MAX_VALUE, MAX_VALUE)
            n = randint(-MAX_VALUE, MAX_VALUE)
            self.assertTrue(testTriple(create_triple(m, n)), "create_triple(" + str(m) + ", " + str(n) + ")")

        # Test random positive values
        for i in range(MAX_TEST_CASES):
            m = randint(1, MAX_VALUE)
            n = randint(1, MAX_VALUE)
            self.assertTrue(testTriple(create_triple(m, n)), "create_triple(" + str(m) + ", " + str(n) + ")")

if __name__ == '__main__':
    unittest.main()
