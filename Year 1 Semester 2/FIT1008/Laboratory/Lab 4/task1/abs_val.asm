        .data
prompt: .asciiz "Please enter an integer: "
return: .asciiz "The absolute value of the number is: "
input:  .word   0

        .text
#print the 'prompt' to prompt users to enter a integer
        la      $a0, prompt         #Load prompt from memory
        addi    $v0, $0, 4          #System Call to print the prompt loaded
        syscall

#get the integer typed by the user
        addi    $v0, $0, 5          #System Call to get user input
        syscall
        sw      $v0, input          #Save the user input in to address "input"

#Check if integer input is positive or negative
        lw      $t0, input          #Load input from memory to register $t0
        blt     $t0, $0, neg        #Check if the input is a negative number

        j       end                 #Go to exit sequence

#If input is negative, change it to positive
neg:    addi    $t1, $0, -1         #Get -1 in the register $t1
        mult    $t0, $t1            #Multiply loaded input by -1
        mflo    $t0                 #Store result back in $t0

        j       end                 #go to exit sequence

#Exit sequence which consists of printing return statement and absolute value
end:    la      $a0, return         #Load return statement
        addi    $v0, $0, 4          #System Call to print $a0
        syscall

        move    $a0, $t0            #load absolute value to $a0
        addi    $v0, $0, 1          #System call to print integer
        syscall

        addi    $v0, $0, 10         #System call to exit program
        syscall

