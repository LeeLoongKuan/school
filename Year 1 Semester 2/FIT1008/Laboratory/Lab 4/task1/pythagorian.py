"""
@description: Creates and print a pythagorian triple based on 2 numbers.

@author: Lee Loong Kuan
@since: 16-08-2015
@modified: 16-08-2015
@param: 2 positive integers, m and n
@param: if input are floats, rounds the numbers down
@return: Returns and pritns a pythagorian triple where a^2 = b^2 + c^2 and
@reutrn: a = |m^2 - n^2|, b = 2mn , c = m^2 + n^2
@precondition: The 2 input numbers must be positive integers
@postcondition: Integers are printed
@complexity: Best and Worst Case: O(1) since we do
@complexity: a fixed amount of operations to get the pythagorian triple

Testing:
Input: 1, 2    Expected: (3, 4, 5)    Result: (3, 4, 5)
Input: 0, 0    Expected: Error        Result: Error
Input: -1, -1  Expected: Error        Result: Error
Input: 'a', 1  Expected: Error        Result: Error
"""


def create_triple(m, n):
    try:
        m = int(m)
        n = int(n)
    except ValueError:
        print("Input is not type int")
        return(-1, -1, -1)

    if (m < 1) or (n < 1):
        print("Input is not positive")
        return(0, 0, 0)

    else:
        a = abs((m ** 2) - (n ** 2))
        print(a)
        b = 2 * m * n
        print(b)
        c = (m ** 2) + (n ** 2)
        print(c)
        return(a, b, c)
