        .data
minput: .asciiz "Please enter value for m: "
ninput: .asciiz "Please enter value for n: "
error:  .asciiz "Error: Input must be positive"
m:      .word   0
n:      .word   0
a:      .word   0
b:      .word   0
c:      .word   0

        .text
#Get the value for m from user
        la      $a0, minput         #Load prompt to ask user for m
        addi    $v0, $0, 4          #System call to print prompt in $a0
        syscall
        addi    $v0, $0, 5          #Sytem call to get input from user
        syscall
        sw      $v0, m              #Store the value obtained

        #Get the value for n from user
        la      $a0, ninput         #Load prompt to ask user for n
        addi    $v0, $0, 4          #System call to print prompt in $a0
        syscall
        addi    $v0, $0, 5          #Sytem call to get input from user
        syscall
        sw      $v0, n              #Store the value obtained

        #Check if m or n is negative numbers, throw error if it is
        lw      $t0, m              #Check if m is negtive, set $t1 to 1 if it is
        slt     $t1, $t0, $0
        lw      $t0, m              #Check if n is negtive, set $t0 to 1 if it is
        slt     $t0, $t0, $0

        or      $t0, $t0, $t1       #If either $t0 or $1 is 1, set $t0 to 1

        beq     $t0, $0, isPos      #Continue with program if $t0 is 0, meaning both are positive

        #Error code to execute when the input is not both positive
        la      $a0, error          #Load prompt with error message
        addi    $v0, $0, 4          #System call to print the prompt
        syscall
        j       exit


isPos:  #Rest of the program, to be excuted if both inputs are positive
        lw      $t0, m              #Load m to register $t0
        lw      $t1, n              #load n to register $t1


        #Calculate a which is |m^2 - n^2|
        mult    $t0, $t0            #$t2 = m^2
        mflo    $t2                 #store result in $t2
        mult    $t1, $t1            #$t3 = n^2
        mflo    $t3                 #store result in $t3

        sub     $t2, $t2, $t3       #$t2 = m^2 - n^2

        #Making $t2 (m^2 - n^2) absolute value, |m^2 - n^2|
        bge     $t2, $0, a_pos      #Check if m^2 - n^2 is a positive number

        #If input is negative, change it to positive
        addi    $t3, $0, -1         #Get -1 in the register $t3
        mult    $t2, $t3            #Multiply m^2 - n^2 by -1
        mflo    $t2                 #Store result back in $t2
        j       a_pos

        #Now that $t2 is an absolute value, store it
a_pos:  sw      $t2, a              #Store in address labeled a


        #Calculate b which is 2mn
        mult    $t0, $t1            #$t2 = m * n
        mflo    $t2

        add     $t2, $t2, $t2       #$t2 = (m*n)+(m*n) which is 2*m*n

        sw      $t2, b              #Store value in address labeled b


        #Calculte c which is m^2 + n^2
        mult    $t0, $t0            #$t2 = m^2
        mflo    $t2                 #store result in $t2
        mult    $t1, $t1            #$t3 = n^2
        mflo    $t3                 #store result in $t3

        add     $t2, $t2, $t3       #$t2 = m^2 + n^2

        sw      $t2, c              #Store value in adress labeled c


        j       print               #Start printing first
newline:#Litte chunk of code to print new line
        addi    $a0, $0, 10
        addi    $v0, $0, 11
        syscall
        jr      $ra

print:  #Print the results to the user

        #print a
        lw      $a0, a
        addi    $v0, $0, 1
        syscall
        jal     newline             #Print new line

        #print b
        lw      $a0, b
        addi    $v0, $0, 1
        syscall
        jal     newline             #Print new line

        #print c
        lw      $a0, c
        addi    $v0, $0, 1
        syscall
        jal     newline             #Print new line


exit:   #exit the program
        addi    $v0, $0, 10
        syscall
