        .data
insize: .asciiz "Please enter the size of the list: "
inele:  .asciiz "Please enter the next integer to add to the list: "
a_list: .word   0
bytes:  .word   4
t_sum:  .word   0

        .text
#Get user to specify size of a list
        la      $a0, insize         #Load the prompt to ask user for size of list
        addi    $v0, $0 ,4          #System call to print prompt loaded
        syscall

        addi    $v0, $0, 5          #System call to get the size the user entered
        syscall
        move    $t0, $v0            #move the size specified to $t0 temporarily

        #Allocate memory for the list
        lw      $t1, bytes          #Load the number of bytes to allocate for each element to $t1
        mult    $t0, $t1            #No of bytes = no of elements * bytes for each element
        addi    $a0, $t0, 4         #Add 4 bytes needed to allocate to store size of list
        addi    $v0, $0, 9          #System call to allocate memory for list
        syscall
        sw      $v0, a_list         #Save address of first byte into a_list
        sw      $t0, ($v0)          #Save the length of the list to the first 4 bytes of the list


#Get user input on integers to put in the list
        lw      $t0, a_list         #Store address of list in $t0
        addi    $t1, $0, 0          #i = 0
        lw      $t2, bytes          #Store number of bytes allocated for each element
        #Loop to ask user what to enter in list
get_ele:
        lw      $t3, ($t0)          #get the length of the list
        bge     $t1, $t3, cont      #if i < length of list

        la      $a0, inele          #Load prompt to ask user for next element
        addi    $v0, $0, 4          #System call to print prompt
        syscall

        addi    $v0, $0, 5          #System call to get user input for integer to enter
        syscall
        move    $t3, $v0            #Store the integer to add to list temporarily in $t3

        mult    $t1, $t2            #address to write = start address + (i * bytes per element + 4)
        mflo    $t4
        add     $t4, $t4, $t0
        addi    $t4, $t4, 4

        sw      $t3, ($t4)          #save element given by user ($t3) to address stored in $t4

        addi    $t1, $t1, 1         # i += 1
        j       get_ele


#Sum the elements in the list until a negative number is reached
cont:   lw      $t0, a_list         #Store address of list in $t0
        add     $t1, $0, $0         #i = 0
        lw      $t2, t_sum          #sum = 0
        lw      $t3, bytes          #Store number of bytes allocated for each element
        lw      $t5, ($t0)          #get length of list
        #loop to sum elements
l_sum:
        bge     $t1, $t5, end       #if i < length of list, continue, else exit loop

        lw      $t4, 4($t0)         #load the element in to the register $t3
        blt     $t4, $0, end        #check if the current element is negative
        add     $t2, $t2, $t4       #if not negative, add to register $t2 to track sum
        add     $t0, $t0, $t3       #add element size to $t0 to move to the next element in list

        addi    $t1, $t1, 1         # i += 1
        j       l_sum


#Print the sum
end:    move    $a0, $t2            #Load the sum to the register $a0
        addi    $v0, $0, 1          #system call to print integer
        syscall

        addi    $v0, $0, 10
        syscall
