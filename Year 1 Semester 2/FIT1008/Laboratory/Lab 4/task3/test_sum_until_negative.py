"""
    @author Brendon Taylor
    @since 20/07/2015
    @modified 20/07/2015
    @modified 08/08/2015 Moved test cases into separate files
    @modified 12/08/2015 Added debug messages
"""

from sum_until_negative import sum_until_negative
import unittest

class TestSumUntilNegative(unittest.TestCase):
    def testSumUntilNegative(self):
        # Test empty list
        self.assertTrue(sum_until_negative([]) == 0, "sum_until_negative([]) == 0")

        # Test positive list
        n = 100
        a_list = list(range(1, n + 1))
        sum = int((n * (n + 1)) / 2)
        self.assertTrue(sum_until_negative(a_list) == sum, "sum_until_negative(" + str(a_list) + ") == " + str(sum))

        # Test negative value at the start
        a_list[0] = -1
        self.assertTrue(sum_until_negative(a_list) == 0, "sum_until_negative(" + str(a_list) + ") == 0")
        a_list = list(range(1, n + 1))

        # Test half way
        n //= 2
        a_list[n] = -1
        sum = int((n * (n + 1)) / 2)
        self.assertTrue(sum_until_negative(a_list) == sum, "sum_until_negative(" + str(a_list) + ") == " + str(sum))

if __name__ == '__main__':
    unittest.main()
