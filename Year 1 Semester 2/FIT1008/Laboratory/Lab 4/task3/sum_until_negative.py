"""
@description: Sum the items in a list from index 0 up to until
the first negative number in the list

@author: Lee Loong Kuan
@since: 17-08-2015
@modified: 17-08-2015
@param: a list of numbers, a_list
@return: The sum of the elements in the list from index 0
@return: to the first negative number in the list
@precondition: none
@postcondition: a value is returned
@complexity: Worst Case: O(n) occurs when all elements are positive
@complexity: where n is the length of the list
@complexity: Best Case: O(1) occurs when the first element is negative

Testing:
Input: [1,-1]   Expected: 1             Result: 1
Input: [-1,1]   Expected: 0             Result: 0
Input: []       Expected: 0             Result: 0
Input: ['a']    Expected: Error         Result: Error
"""


def sum_until_negative(a_list):
    sum = 0
    for no in a_list:
        if no < 0:
            return sum
        else:
            sum += no
    return sum
