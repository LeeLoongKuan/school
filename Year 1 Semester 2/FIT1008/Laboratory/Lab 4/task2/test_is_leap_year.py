"""
    @author Brendon Taylor
    @since 20/07/2015
    @modified 20/07/2015
    @modified 08/08/2015 Moved test cases into separate files
    @modified 12/08/2015 Added debug messages and only used years >= 1582
"""

from is_leap_year import is_leap_year
import unittest

class TestIsLeapYear(unittest.TestCase):
    def testIsLeapYear(self):
        test_data_true = [1600, 2000, 2012]
        test_data_false = [1582, 1700, 1800, 1900, 2003, 2100]

        for year in test_data_true:
            self.assertTrue(is_leap_year(year), "is_leap_year(" + str(year) + ")")

        for year in test_data_false:
            self.assertFalse(is_leap_year(year), "is_leap_year(" + str(year) + ")")

if __name__ == '__main__':
    unittest.main()
