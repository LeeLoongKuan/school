"""
@description: Checks if a year greater than equal to 1582 is a leap year

@author: Lee Loong Kuan
@since: 17-08-2015
@modified: 17-08-2015
@param: a year in the form of integer
@param: if the year is a float, rounds the year down
@return: Returns True is is a leap year and prints "Is a leap year"
@return: Returns False otherwise and prints "Is not a leap year"
@precondition: The year entered is an integer greater than equal to 1582 
@postcondition: A boolean value is returned and text is printed
@complexity: Best and Worst Case: O(1) since we do a fixed amount of
@complexity: operations to determine if the year is a leap year or not

Testing:
Input: 400      Expected: True          Result: True
Input: 600      Expected: False         Result: False
Input: 204      Expected: True          Result: True
Input: 'a'      Expected: Error         Result: Error
"""


def is_leap_year(year):
    leapYear = False
    if year % 400 == 0:
        leapYear = True
    elif (year % 4 == 0) and (year % 100 != 0):
        leapYear = True
    else:
        leapYear = False

    if year < 1582:
        leapYear = False

    if leapYear == True:
        print("Is a leap year")
    else:
        print("Is not a leap year")

    return leapYear
