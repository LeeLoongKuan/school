        .data
prompt: .asciiz "Please enter a year: "
year:   .word   0
Yes:    .asciiz "Is a leap year"
No:     .asciiz "Is not a leap year"
Error:  .asciiz "Error: The year must be greater than or equal to 1582"

        .text
#Get input from user
        #Print prompt to ask user to enter a year to check
        la      $a0, prompt         #Load the prompt
        addi    $v0, $0, 4          #System call to print prompt loaded
        syscall
        #Get the year entered by user
        addi    $v0, $0, 5          #System call to get integer user entered
        syscall
        sw      $v0, year           #Save the year entered by the user

#Check if year is greater than equal to 1582
        lw      $t0, year
        addi    $t1, $0, 1582
        blt     $t0, $t1, error

#Check if the year is a leap year
        lw      $t0, year
        #Check if year is divisible by 400
        addi    $t1, $0, 400
        rem     $t1, $t0, $t1
        beq     $t1, $0, it_is

        #Check if year is divisible by 4 but not 100
        addi    $t1, $0, 4          #Check for divisibility by 4 first
        rem     $t1, $t0, $t1
        bne     $t1, $0, is_not     #If the remainder is not 0, we know it is not leap year

        addi    $t1, $0, 100        #Else, Check for divisibility by 100
        rem     $t1, $t0, $t1
        beq     $t1, $0, is_not     #Fi the remainder is 0, we know it is not leap year

it_is:  #If we haven't jumped to the case where is not leap year, it is leap year
        la      $a0, Yes            #Load prompt that says it is a leap year and print it
        addi    $v0, $0, 4
        syscall
        j       exit                #Exit program

is_not: #This is the case where the year entered is not a leap year
        la      $a0, No             #Load prompt that says it is not a leap year and print it
        addi    $v0, $0, 4
        syscall
        j       exit                #Exit program

error:  #Prints error statement if the year is not >= 1582
        la      $a0, Error          #Load and print the error prompt
        addi    $v0, $0, 4
        syscall
        j       exit                #Exit Program

exit:
        addi    $v0, $0, 10
        syscall
