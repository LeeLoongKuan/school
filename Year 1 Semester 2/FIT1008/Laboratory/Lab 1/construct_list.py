"""
@description: Constructs a list based on the inputs of the user
Asks for the size of the list the user want to create
Asks for each element to be put into the list

@author: Lee Loong Kuan
@since: 29-07-2015
@modified: 29-07-2015
@param: the number of values in the list; the elements to be put into the list

Testing:
input: 2, 1, 2    expected: [1,2]    result: [1,2]
input: 0          expected: []       result: []
input: a          expected: error    result: error
input: -1         expected: []       result: []
input: ""            expected: []       result: []
"""
size = int(input("Enter number of values: "))
the_list = []

for i in range(size):
    the_list.append(int(input("Value: ")))

print(the_list)
