"""
@description: Checks is the year given is a leap year

@author: Lee Loong Kuan
@since: 29-07-2015
@modified: 29-07-2015
@param: The year to check if is a leap year

Testing:
input: 1999  expected: is NOT a leap year  result: is NOT a leap year
input: a     expected: error               result: error
input: 2000  expected: is a leap year      result: is a leap year
input: -4    expected: is a leap year      result: is a leap year
input: -3    expected: is NOT a leap year  result: is NOT a leap year
input: ""    expected: error               result: error 
"""
year = int(input('Enter year: '))

if ((year % 4 == 0) and (year % 100 != 0)) or (year % 400 == 0):
    print(year, 'is a leap year')
else:
    print(year, 'is NOT a leap year')
