"""
@descripton: Implements a 4X4 puzzle
Allows user to move pieces in the puzzle.

@author: Lee Loong Kuan
@since: 28-07-2015
@modified: 29-07-2015
@param: The move to do. Up, Down, Left, or Right
@return: Changes and prints the state of the puzzle
"""

from tkinter import *


class PuzzleClass:
    def __init__(self):
        self.puzzle = [[]]
        self.represent = ""
        self.empPos = (-1, -1)

        self.reset()

    def reset(self):
        self.puzzle = [[' 1', ' 2', ' 3', ' 4'],
                       [' 5', ' 6', ' 7', ' 8'],
                       [' 9', '10', '11', '12'],
                       ['13', '14', '15', ' X']]
        self._create_represent()
        self.empPos = (3, 3)

    def _create_represent(self):
        """Create a representation the puzzle in a better format"""
        rep = ""
        for i in self.puzzle:
            rep += "|"
            for j in i:
                rep += str(j) + " "
            rep += "|\n"
        self.represent = rep

    def _empty_pos(self):
        """Returns the current position of the empty spot in the given puzzle"""
        i = 0
        while i < len(self.puzzle):
            j = 0
            while j < len(self.puzzle):
                if self.puzzle[i][j] == " X":
                    self.empPos = (i, j)
                j += 1
            i += 1
        self.empPos = (i, j)

    def _swap(self, pos1, pos2):
        """Swaps the element at pos1 and pos2 of puzzle"""
        swap_tmp = self.puzzle[pos1[0]][pos1[1]]
        self.puzzle[pos1[0]][pos1[1]] = self.puzzle[pos2[0]][pos2[1]]
        self.puzzle[pos2[0]][pos2[1]] = swap_tmp

    def print_puzzle(self):
        print(self.represent)

    def move(self, dir_to_move):
        next_empty = (self.empPos[0] + dir_to_move[0], self.empPos[1] + dir_to_move[1])
        if (next_empty[0] < 0 or next_empty[0] > 3 or next_empty[1] < 0 or next_empty[1] > 3):
            print("Error: Invalid Move")
        else:
            self._swap(self.empPos, next_empty)
            self.empPos = next_empty
        self._create_represent()


directions = {'w': (-1, 0), 's': (1, 0),
              'a': (0, -1), 'd': (0, 1), 'q': None}
"""
while(1):
    dir = input("pls give me dir: ")
    if dir in directions:
        dir_to_move = directions[dir]
        puzz.move(dir_to_move)
        print(puzz.represent)
    else:
        print("bad input")
"""


class App:
    def __init__(self, master):
        self.puzzleText = StringVar()

        self.puzzleDisplay = Label(master, justify=LEFT,
                                   textvariable=self.puzzleText,
                                   font=("Courier", 16))
        self.puzzleDisplay.pack()

        self.moves = {'w': (-1, 0), 'a': (0, -1),
                      's': (1, 0), 'd': (0, 1), 'q': None}
        self.puzz = PuzzleClass()
        self.puzzleText.set(self.puzz.represent)

        master.bind('<Up>', self.Up)
        master.bind('<Down>', self.Down)
        master.bind('<Left>', self.Left)
        master.bind('<Right>', self.Right)

    def Up(self, event):
        self.puzz.move((-1, 0))
        self.puzzleText.set(self.puzz.represent)

    def Down(self, event):
        self.puzz.move((1, 0))
        self.puzzleText.set(self.puzz.represent)

    def Left(self, event):
        self.puzz.move((0, -1))
        self.puzzleText.set(self.puzz.represent)

    def Right(self, event):
        self.puzz.move((0, 1))
        self.puzzleText.set(self.puzz.represent)

master = Tk()

app = App(master)

master.mainloop()
master.destroy()
