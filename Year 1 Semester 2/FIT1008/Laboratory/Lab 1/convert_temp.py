"""
@description: Converts a temperature from Celcius to Farenheit

@author: Lee Loong Kuan
@since: 29-07-2015
@modified: 29-07-2015
@param: The temperature in Celcius

Testing:
input: 9        expected: 48          result: 48
input: 0        expected: 32          result: 32
input: a        expected: error       result: error
input: -1       expected: 30          result: 30
input: ""       expected: error       result: error 
"""
temp_C = int(input('Enter temperature in Celsius '))

temp_F = int(9*temp_C/5 + 32)

print('Temperature in Fahrenheit is ' + str(temp_F))
