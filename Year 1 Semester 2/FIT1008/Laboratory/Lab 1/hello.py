"""
@description: Gets the user's name and prints a greeting message with the name

@author: Lee Loong Kuan
@since: 29/07/2015
@modified: 29/07/2015
@param: user's name

Testing:
input: "asd"    expected: Hello asd. Welcome    result: Hello asd. Welcome
input: ""         expected Hello . Welcome        result: Hello . Welcome
"""

name = input('Enter name (max 60 chars): ')
print('Hello ' + name + '. Welcome')
