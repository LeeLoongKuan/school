(TeX-add-style-hook
 "Graph_Explaination"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english")))
   (TeX-run-style-hooks
    "latex2e"
    "./tex/Graph2"
    "./tex/Graph3"
    "./tex/Adv_Ques"
    "./tex/Hall_of_Fame"
    "jhwhw"
    "jhwhw10"
    "babel"
    "amsthm"
    "amsmath"
    "subfiles"
    "pgf"
    "tikz"
    "mathrsfs"
    "graphicx")))

