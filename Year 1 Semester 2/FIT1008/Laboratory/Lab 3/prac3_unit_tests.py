"""
    @author Brendon Taylor
    @since 18/07/2015
    @modified 20/07/2015
    @modified 03/08/2015 Added support for Python 3
"""

from prac3 import sum_items, shaker_sort
from copy import deepcopy
from random import randint, shuffle
import unittest

class TestPracThree(unittest.TestCase):
    def testSumItems(self):
        MAX_VALUE = 1000
        MAX_TEST_CASES = 100

        # Test empty list
        self.assertTrue(sum_items([]) == 0)

        # Test non empty lists
        for i in range(MAX_TEST_CASES):
            n = randint(1, MAX_VALUE)
            sum = int((n * (n + 1)) / 2)
            self.assertTrue(sum == sum_items(list(range(1, n + 1))))

    def testShakerSort(self):
        MAX_VALUE = 100
        MAX_TEST_CASES = 10

        # Test empty list
        a_list = []
        shaker_sort(a_list)
        self.assertTrue(a_list == [])

        # Test sorted list
        a_list = list(range(1, MAX_VALUE))
        old_list = deepcopy(a_list)
        shaker_sort(a_list)
        self.assertTrue(a_list == old_list)

        # Test unsorted lists
        old_list = deepcopy(a_list)
        for i in range(MAX_TEST_CASES):
            shuffle(a_list)
            shaker_sort(a_list)
            self.assertTrue(a_list == old_list)

if __name__ == '__main__':
    unittest.main()