""""
@description: A module to assist in plotting graphs of the time complexity

@author: Lee Loong Kuan
@since: 09-08-2015
@modified: 10-08-2015
"""

from matplotlib import pyplot
import json
import sys


def plot_data(data_set, fname):
    """
    @description: A function to plot a data set and save it as a file

    @param: a list or tuple of 2 list. ([x-data], [y-data])
    @param: the name to save the plot as, fname
    @return: A file is saved that contains the plot
    @precondition: the data_set is of the correct format
    @postcondition: a file is created
    @complexity: Best and Worst Case: O(n)
    @complexity: where n is the length of the data set
    """
    fig = pyplot.figure()
    fig.clf()
    for data_index in range(1, len(data_set)):
        pyplot.plot(data_set[0], data_set[data_index], linestyle='solid',
                    marker='o', markerfacecolor='red')
    fig.suptitle(fname, fontsize=20)
    pyplot.xlabel('list length, n', fontsize=14)
    pyplot.ylabel('time taken, seconds', fontsize=14)
    fig.savefig(fname)


def plot_all():
    json_in = open("json_all_functions")
    data_set = json.load(json_in)
    plot_data([data_set[0], data_set[1]], "Standard Find Interval")
    plot_data([data_set[0], data_set[2]], "Quick Find Interval")
    plot_data([data_set[0], data_set[3]], "Compressed Quick Find Interval")
    plot_data([data_set[0], data_set[1], data_set[2]],
              "Standard vs Quick Find Interval")
    plot_data([data_set[0], data_set[2], data_set[3]],
              "Quick vs Compressed Find Interval")


def plot_all_avg():
    json_in = open("json_avg_all_functions")
    data_set = json.load(json_in)
    plot_data([data_set[0], data_set[1]],
              "Average Standard Find Interval")
    plot_data([data_set[0], data_set[2]],
              "Average Quick Find Interval")
    plot_data([data_set[0], data_set[3]],
              "Average Compressed Quick Find Interval")
    plot_data([data_set[0], data_set[1], data_set[2]],
              "Average Standard vs Quick Find Interval")
    plot_data([data_set[0], data_set[2], data_set[3]],
              "Average Quick vs Compressed Find Interval")
