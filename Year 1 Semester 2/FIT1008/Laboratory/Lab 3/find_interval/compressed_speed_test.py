"""
@description: A module which contains funcions to time all the
different find_interval modules

@author: Lee Loong Kuan
@since: 09-08-2015
@modified: 10-08-2015
"""
import json
import timeit
import random
import find_interval_functions as fi_func


def run_all(n):
    """
    @description: This function is to test if all the find_interval functions
    actually finds and returns the same interval for the same list.
    This function is purely for checking and debugging purposes

    @param: the length of the list for the functions to run on
    @return: prints the interval found by each function
    @precondition: none
    @postcondition: none
    @complexity: Dependent on the complexity of the functions called
    """
    # create random list of positive and negative real numbers n long
    rand_list = []
    for _ in range(n):
        if random.random() > 0.5:
            rand_list.append(random.random())
        else:
            rand_list.append(random.random() * -1)
    # Run all the functions and print their output
    print("norm:", fi_func.find_interval(rand_list[:]))
    print("qick:", fi_func.quick_find_interval(rand_list[:]))
    print("comp:", fi_func.compressed_quick_find_interval(rand_list[:]))


def time_find_interval(a_list):
    """
    @description: Mesures the time taken for quick_find_interval
                  to find the interval with the gratest sum in a_list

    @param: a list of real numbers, a_list
    @return: time taken for quick_find_interval to find
             the interval in a_list with the gretest sum
    @precondition: a_list must only contain real numbers
    @postcondition: none
    @complexity: Best and Worst Case: O(n)
                 where n is the complexity of quick_find_interval
    """
    taken = []
    start = timeit.default_timer()
    #fi_func.quick_find_interval(a_list[:])
    taken.append((timeit.default_timer() - start))
    start = timeit.default_timer()
    fi_func.compressed_quick_find_interval(a_list[:])
    taken.append((timeit.default_timer() - start))
    return taken


def table_time_find_interval():
    """
    @description: Generates lists of size from 2 to 1024 with a step of 2*n
    Runs all the lists generated through time_quick_find_interval.
    Save the size of list and the output of time_quick_find_interval
    in a text file.

    @param: none
    @return: Creates a text file called "output_quick_find_interval.txt"
             Contains the list size and output from time_quick_find_interval
    @return: Creates a json file with the raw ouputs to be used in plotting
             the graph of the time taken
    @precondition : none
    @postcondition: a file named output_quick_find_interval.txt is created
    @complexity: Best and Worst Case: O(n)
                 where n is the complexity of quick_find_interval
    """
    json_out = open("json_all_functions", "w")
    text_out = open("output_all_functions.txt", "w")
    print("=========================================", file=text_out)
    print("| n value | norm_fi |  quick  | compres |", file=text_out)
    print("=========================================", file=text_out)
    random.seed()
    norm_time_taken = []
    quick_time_taken = []
    compressed_time_taken = []
    power_of_2 = [2**n for n in range(1, 11)]
    for n in power_of_2:
        rand_no_list = []
        # generate list of random numbers of size n
        for _ in range(n):
            if random.random() > 0.5:
                rand_no_list.append(random.random())
            else:
                rand_no_list.append(random.random() * -1)
        print("Processing list of length ", n)
        time_taken = time_find_interval(rand_no_list)
        norm_time_taken.append(time_taken[0])
        quick_time_taken.append(time_taken[1])
        compressed_time_taken.append(time_taken[2])
        print(n, time_taken[0], time_taken[1], time_taken[2], file=text_out)

    json.dump([power_of_2, norm_time_taken, quick_time_taken,
               compressed_time_taken], json_out)

    text_out.close()
    json_out.close()


def table_avg_time_find_interval():
    """
    @description: Generates 100 lists of size n
    Where n is a number in the sequece 2, 4, 8, 16, ..., 1024
    Runs all the lists per n generated through time_quick_find_interval.
    Averages all the output of time_quick_find_interval per n length.
    Save the size of list and the corresponding output
    of time_quick_find_interval
    in a text file.

    @param: none
    @return: Creates a text file called "output_avg_quick_find_interval.txt"
             Contains the list size and output from time_quick_find_interval
    @return: Creates a json file with the raw ouputs to be used in plotting
             the graph of the time taken
    @precondition : none
    @postcondition: a file named output_avg_quick_find_interval.txt is created
    @complexity: Worst Case: O(n)
                 where n is the Worst case complexity of quick_find_interval
    @complexity: Best Case : O(m)
                 where m is the Best Case complexity of quick_find_interval
    """
    json_out = open("json_avg_all_functions", "w")
    text_out = open("output_avg_all_functions.txt", "w")
    print("=========================================", file=text_out)
    print("| n value | norm_fi |  quick  | compres |", file=text_out)
    print("=========================================", file=text_out)
    random.seed()
    norm_time_taken = []
    quick_time_taken = []
    compressed_time_taken = []
    power_of_2 = [2**n for n in range(1, 11)]
    for n in power_of_2:
        # generate 100 lists of random numbers of size n
        sum = [0,0]
        avg = [0,0]
        for i in range(100):
            rand_no_list = []
            print("Processing list:", i + 1, "of size:", n)
            for _ in range(n):
                if random.random() > 0.5:
                    rand_no_list.append(random.random())
                else:
                    rand_no_list.append(random.random() * -1)
            time_taken = time_find_interval(rand_no_list)
            sum[0] += time_taken[0]
            sum[1] += time_taken[1]

        avg[0] = sum[0]/100
        avg[1] = sum[1]/100
        quick_time_taken.append(avg[0])
        compressed_time_taken.append(avg[1])
    
    for i in range(len(power_of_2)):
        print(power_of_2[i], compressed_time_taken[i])

    json.dump([power_of_2, quick_time_taken, compressed_time_taken], json_out)

    text_out.close()
    json_out.close()


table_avg_time_find_interval()
