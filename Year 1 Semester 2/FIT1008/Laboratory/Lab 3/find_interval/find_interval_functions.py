""""
@description: A module which contains functions to:
Find the interval in a list with the greatest sum
and functions which assist in that

@author: Lee Loong Kuan
@since: 06-08-2015
@modified: 11-08-2015
"""


def check_greatest(largest_tup, tmp_tup):
    """
    @description: checks which set of intervals has the greatest sum
    if both intervals have the same sum, then choose the smallest interval

    @param: 2 tuples of size 3 with the format (interval_sum, i_min, i_max)
    @param: the first tuple is for the established greatest interval
    @param: the second being the new interval to check if its the greatest
    @return: the interval with the greatest sum or the smallest interval
    @precondition: the tuples are of the correct format and numerical
    @postcondition: none
    @complexity: O(1) as it will a take constant time

    Testing:
    Input: (1,0,3),(2,0,4)      Expected: (2,0,4)    Result:(2,0,4)
    Input: (1,0,3),(1,0,2)      Expected: (1,0,2)    Result:(1,0,2)
    Input: ('a',1,1),(2,1,1)    Expected: Error      Result: Error
    Input: (1,0,3),(1,0,3)      Expected: (1,0,3)    Result:(1,0,3)
    Input: (-1,0,1),(-2,0,0)    Expected: (-1,0,1)   Result:(-1,0,1)
    """
    if largest_tup[0] < tmp_tup[0]:
        return tmp_tup
    elif largest_tup[0] == tmp_tup[0]:
        if (largest_tup[2] - largest_tup[1]) < (tmp_tup[2] - tmp_tup[1]):
            return largest_tup
        else:
            return tmp_tup
    else:
        return largest_tup


def find_interval(a_list):
    """
    @description: finds the interval in the a_list with the greatest sum by
    interating over all possible intervals in a_list

    @param: a list of real numbers, a_list
    @return: the min and max interval in a_list with the greatest sum
    @return: returns in the format [i_min, i_max]
    @precondition: a_list must only contain real numbers and be non empty
    @postcondition: none
    @complexity: Best and Worst Case: O(n^2) where n is the length of a_list
                 Because it interates over all interval possiblities

    Testing:
    Input: [-1,1,-2]    Expected: [1,1]    Result: [1,1]
    Input: [1,-9,5]     Expected: [2,2]    Result: [2,2]
    Input: []           Expected: Error    Result: Error
    Input: [-1, -2]     Expected: [0,0]    Result: [0,0]
    """
    if len(a_list) < 1:
        raise NameError("Can't find interval in list size 0")
    largest_sum = 0
    i_min = 0
    i_max = 0

    for i in range(len(a_list)):
        tmp_min = i
        for j in range(i, len(a_list)):
            tmp_max = j
            tmp_sum = 0
            # for loop to sum all elements from i to j
            for k in range(i, j+1):
                tmp_sum += a_list[k]

            # Check if the current interval is the greatest
            greatest_interval = check_greatest((largest_sum, i_min, i_max),
                                               (tmp_sum, tmp_min, tmp_max))
            largest_sum = greatest_interval[0]
            i_min = greatest_interval[1]
            i_max = greatest_interval[2]

    return [i_min, i_max]


def quick_find_interval(a_list):
    """
    @description: A variant of find_interval from advanced question
    Uses concepts from dynamic prgramming to optimise the algorithm
    The optimisation allows for less operations to be done.

    How it works (examples):
    input: [1,2,3,4,5]
    after loop 1: [1,3,6,10,15] each element contains the sum from
                                the first element to 
                                the element that occupied that index in a_list
    after loop 2: [1], [2,5,9,14] each element contains the sum from
                                  the second element till
                                  the element that occupied that index in a_list
                                  acheives this by popping first element
                                  subtracts all element by first element

    @param: a list of real numbers, a_list
    @return: the min and max interval in a_list with the greatest sum
    @precondition: a_list must only contain real numbers
    @postcondition: a_list is now an emptylist
    @complexity: Best and Worst Case: O(n^2) where n is the length of a_list

    Testing:
    Input: [-1,1,-2]    Expected: [1,1]    Result: [1,1]
    Input: [1,-9,5]     Expected: [2,2]    Result: [2,2]
    Input: []           Expected: Error    Result: Error
    Input: [-1, -2]     Expected: [0,0]    Result: [0,0]
    """
    if len(a_list) < 1:
        raise NameError("Can't find interval in list size 0")
    largest_sum = a_list[0]
    i_min = 0
    i_max = 0

    # Create a list of the sum we can get if take i_min as first element
    tmp_min = 0
    tmp_sum = 0
    for i in range(1, len(a_list)):
        tmp_max = i
        a_list[i] = a_list[i] + a_list[i - 1]
        tmp_sum = a_list[i]
        # Check if the current interval is the greatest
        greatest_interval = check_greatest((largest_sum, i_min, i_max),
                                           (tmp_sum, tmp_min, tmp_max))
        largest_sum = greatest_interval[0]
        i_min = greatest_interval[1]
        i_max = greatest_interval[2]

    # slowly remove one element at a time form the list of sums
    # this loop essentially increaments i_min
    for i in range(len(a_list)):
        removed_element = a_list.pop(0)
        tmp_min = i + 1
        for j in range(len(a_list)):
            tmp_max = i + 1 + j
            a_list[j] = a_list[j] - removed_element
            tmp_sum = a_list[j]

            # Check if the current interval is the greatest
            greatest_interval = check_greatest((largest_sum, i_min, i_max),
                                               (tmp_sum, tmp_min, tmp_max))
            largest_sum = greatest_interval[0]
            i_min = greatest_interval[1]
            i_max = greatest_interval[2]

    return [i_min, i_max]


def compress_list(a_list):
    """
    @description: compresse the list by clustering all the elements in a list.
    Clusters it by creating another list consisting of the sum of individual
    positive and negative clusters in a_list.
    This method of compressing the list essentially allows us to give
    find_interval a smaller input without lossing any data pertinant to
    finding the interval with the greatest sum (most of the time)

    There is an edge case where if all the elements in the list are negative
    the list returned will not contain sufficient information for find_interval
    to find the maximum sum interval
    (it'll reutrn an interval which is the whole list)
    So to compensate we add a special case where if compressed list only has
    one negative cluster, we just return the index of the biggest negative no.

    @param: a list of real numbers, a_list
    @return: a tuple consisting of:
    @return: a compressed version of a_list
    @return: the interval from a_list each cluster represents
    @return: the index of the greatest element in a_list
    if and only if all elements in list is negative
    @precondition: a_list must only contain real numbers and be non empty
    @postcondition: none
    @complexity: Best and Worst Case: O(n) where n is the length of a_list
    
    Testing:
    Input: [1,2,-1]    Expected: ([3,-1], [(0,1),(2,2)], -1)
                       Result  : ([3,-1], [(0,1),(2,2)], -1)
    Input: [-1,-2]     Expected: ([-3], [(0,1)], 0)
                       Result  : ([-3], [(0,1)], 0)
    Input: [0]         Expected: ([0], [(0,0)], -1)
                       Result  : ([0], [(0,0)], -1)
    Input: []          Expected: Error
                       Result  : Error
    """
    if len(a_list) < 1:
        raise NameError("Can't compress list any further")

    i_greatest_element = 1
    compressed_list = []
    interval_cluster = []
    i_min = 0
    i_max = 0
    tmp_sum = 0

    for i in a_list:
        if (tmp_sum > 0 and i < 0) or (tmp_sum < 0 and i > 0):
            compressed_list.append(tmp_sum)
            interval_cluster.append((i_min, i_max - 1))
            i_min = i_max
            tmp_sum = 0
        tmp_sum += i
        i_max += 1
    compressed_list.append(tmp_sum)
    interval_cluster.append((i_min, i_max - 1))

    # in the edge case of all negative no, find greatest element
    if len(compressed_list) == 1 and compressed_list[0] < 0:
        i_greatest_element = 0
        for j in range(len(a_list)):
            if a_list[i_greatest_element] < a_list[j]:
                i_greatest_element = j

    return (compressed_list, interval_cluster, i_greatest_element)


def convert_interval(interval_cluster, interval_to_convert):
    """
    @description: Converts the interval in terms of the compressed list
    to in terms of the original list

    @param: A list of the intervals of each cluster
    @param: the interval in terms of the clustered list to convert
    @return: the interval in terms of the original list
    @precondition: both input list are of the correct format
    @precondition: values in interval_to_convert are
    within the range of the number of tuples in interval_cluster
    @precondition: both of the input must be non empty
    @precondition: the function compressed_list is already runed
    @postcondition: none
    @complexity: O(1) as we just return 2 elements in the interval_cluster list
    
    Testing:
    Input: [(0,1),(2,3)],[0,1]    Expected: [0,3]    Result: [0,3]
    Input: [(0,1),(2,3)],[0,0]    Expected: [0,1]    Result: [0,1]
    Input: [(0,1)],[0,1]          Expected: Error    Result: Error
    Input: [(0,0)],[0,0]          Expected: Error    Result: Error
    Input: [],[]                  Expected: Error    Result: Error
    """
    i_min = interval_cluster[interval_to_convert[0]][0]
    i_max = interval_cluster[interval_to_convert[1]][1]
    return[i_min, i_max]


def compressed_quick_find_interval(a_list):
    """
    @description: Mesures the time taken for quick_find_interval
    to find the interval with the gratest sum in a_list

    @param: a list of real numbers, a_list
    @return: time taken for quick_find_interval to find
             the interval in a_list with the gretest sum
    @precondition: a_list must only contain real numbers
    @postcondition: a_list is now empty list
    @complexity: Best Case: O(n) where n is the length of a_list
                 Occurs when all elements in list is either positive or negative
    @complexity: Worst Case: O(n^2) where n is the length of a_list
                 Occurs when all elemnts in list alternates between + and -

    Testing:
    Input: [-1,1,-2]    Expected: [1,1]    Result: [1,1]
    Input: [1,-9,5]     Expected: [2,2]    Result: [2,2]
    Input: []           Expected: Error    Result: Error
    Input: [-1, -2]     Expected: [0,0]    Result: [0,0]
    """
    compressed_list_info = compress_list(a_list)
    if len(a_list) < 1:
        raise NameError("Can't find interval in list size 0")
    elif compressed_list_info[2] < 0:
        return [compressed_list_info[2], compressed_list_info[2]]
    interval_to_convert = quick_find_interval(compressed_list_info[0])
    converted_interval = convert_interval(compressed_list_info[1],
                                          interval_to_convert)
    return converted_interval
