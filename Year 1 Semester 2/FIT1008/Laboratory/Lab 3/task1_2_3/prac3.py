"""
@description: A module which contains functions to:
Sum up all the elements in a list and
measures the time taken to sum up all the elements in the list

A shaker sort implementation and a function to time it

@author: Lee Loong Kuan
@since: 05-08-2015
@modified: 10-08-2015
"""


import json
import timeit
import random


def sum_items(a_list):
    """
    @description: Sums up all the elements in a list

    @param: a list of real numbers, a_list
    @return: the sum of all the elements of a_list
             0 if the list if empty
    @precondition: a_list must only contain real numbers
    @postcondition: none
    @complexity: Best and Worst Case: O(n)
                 where n is the number of elements in a_list
                 because we only have to iterate on every element once

    Testing:
    Input: [1,2,3]      Expected: 6        Result: 6
    Input: []           Expected: 0        Result: 6
    Input: ['a','b']    Expected: Error    Result: Error
    Input: [2, -1]      Expected: 1        Result: 1
    """
    total_sum = 0
    for i in a_list:
        total_sum += i
    return total_sum


def time_sum_items(a_list):
    """
    @description: Measures the time taken for function sum_items to run

    @param: a list of real numbers, a_list
    @return: The time taken for sum_items run on the list a_list
    @precondition: a_list must only contain real numbers
    @postcondition: none
    @complexity: Best and Worst Case: O(m)
                 where m is complexity of sum_items
    """
    start = timeit.default_timer()
    sum_items(a_list)
    taken = (timeit.default_timer() - start)
    return taken


def table_time_sum_items():
    """
    @description: Generates lists of size from 2 to 1024 with a step of n*2
    Runs all the lists generated through time_sum_items.
    Save the size of list and the corresponding output of time_sum_items
    in a text file.

    @param: none
    @return: Creates a text file called "prac3_output.txt"
             Contains the list size and output from time_sum_items
    @return: Creates a json file with the raw ouputs to be used in plotting
             the graph of the time taken
    @precondition : none
    @postcondition: a file named prac3_output.txt is modified/created
    @complexity: Best and Worst Case: O(m)
                 where m is the complexity of time_sum_items
    """
    json_output = open("json_sum_items", "w")
    text_output = open("prac3_output.txt", "w")
    random.seed()
    time_list = []
    power_of_2 = [2**n for n in range(1, 11)]
    for n in power_of_2:
        rand_no_list = []
        # generate list of random numbers of size n
        for _ in range(n):
            rand_no_list.append(random.random())
        time_taken = time_sum_items(rand_no_list)
        print(n, time_taken, file=text_output)
        time_list.append(time_taken)

    json.dump([power_of_2, time_list], json_output)
    text_output.close()
    json_output.close()


def swap(point1, point2, working_list):
    """
    @description: swaps element at point1 with element at point2 in
    working_list

    @param: a list of elements, working_list
    @param: 2 indecies within the list, working_list
    @return: Working_list with elements at point1 and 2 swapped
    @precondition: point1 and point2 must be indices within the list
    @postcondition: input list has 2 elements swapped
    @complexity: Best and Worst Case: O(1) since we just swap 2 elements
    no matter the size of list or indecies

    Testing:
    Input:(0,2,[1,2,3])    expected:[3,2,1]    result: [3,2,1]
    Input:(0,0,[])         expected: Error     result: Error
    Input:('a',0,[])       expected: Error     result: Error
    """
    tmp = working_list[point1]
    working_list[point1] = working_list[point2]
    working_list[point2] = tmp


def shaker_sort(a_list):
    """
    @description: Uses shaker sort to sort a_list
    Shaker sort is a type of bubble sort that
    alternates between moving the largest element and the smallest element
    Note: The function terminates and return the sorted list
          as soon as the list is sorted

    @param: a list of orderable elements, a_list
    @return: A sorted a_list
    @precondition: a_list must only contain orderable elements
    @postcondition: a_list is now sorted
    @complexity: Worst Case: O(n^2) where n is the length of a_list
                             Occurs When list is reverse sorted
    @complexity: Best Case : O(n) where n is the length of a_list
    @complecity: Best Case : Occurs for a list thats alreasy sorted

    Testing:
    Input: [-1,0,1]    Expected: [-1,0,1]    Result: [-1,0,1]
    Input: [1,0,-1]    Expected: [-1,0,1]    Result: [-1,0,1]
    Input: []          Expected: []          Result: []
    Input: ['b','a']   Expected: ['a','b']   Result: ['a','b']
    """
    direc = 1
    is_sorted = False
    ls_pnt = 0
    while not is_sorted:
        is_sorted = True
        for _ in range(len(a_list) - 1):
            ls_pnt += direc
            prev_pnt = ls_pnt - direc
            if (a_list[prev_pnt] * direc) > (a_list[ls_pnt] * direc):
                swap(ls_pnt, prev_pnt, a_list)
                is_sorted = False

        direc = direc * -1


def time_shaker_sort(a_list):
    """
    @description: Mesures the time taken for
                  shaker_sort to sort a_list

    @param: a list of orderable elements, a_list
    @return: time taken for shaker_sort to sort a_list
    @precondition: a_list must only contain orderable elements
    @postcondition: a_list is now sorted
    @complexity: Worst Case: O(n)
                 where n is the Worst case complexity of shaker_sort
    @complexity: Best Case : O(m)
                 where m is the Best Case complexity of shaker_sort
    """
    start = timeit.default_timer()
    shaker_sort(a_list)
    taken = (timeit.default_timer() - start)
    return taken


def table_time_shaker_sort():
    """
    @description: Generates lists of size from 2 to 1024 with a step of 2*n
    Runs all the lists generated through time_shaker_sort.
    Save the size of list and the corresponding output of time_shaker_sort
    in a text file.

    @param: none
    @return: Creates a text file called "output_shaker_sort.txt"
             Contains the list size and output from time_shaker_sort
    @return: Creates a json file with the raw ouputs to be used in plotting
             the graph of the time taken
    @precondition : none
    @postcondition: a file named output_shaker_sort.txt is modified/created
    @complexity: Worst Case: O(n)
                 where n is the Worst case complexity of shaker_sort
    @complexity: Best Case : O(m)
                 where m is the Best Case complexity of shaker_sort
    """
    json_output = open("json_shaker_sort", "w")
    text_output = open("output_shaker_sort.txt", "w")
    random.seed()
    time_list = []
    power_of_2 = [2**n for n in range(1, 11)]
    for n in power_of_2:
        rand_no_list = []
        # generate list of random numbers of size n
        for _ in range(n):
            rand_no_list.append(random.random())
        time_taken = time_shaker_sort(rand_no_list)
        time_list.append(time_taken)
        print(n, time_taken, file=text_output)

    json.dump([power_of_2, time_list], json_output)
    text_output.close()
    json_output.close()


def table_avg_time_shaker_sort():
    """
    @description: Generates 100 lists of size n
    Where n is a number in the sequece 2, 4, 8, 16, ..., 1024
    Runs all the lists per n generated through time_shaker_sort.
    Averages all the output of time_shaker_sort per n length.
    Save the size of list and the corresponding output of time_shaker_sort
    in a text file.

    @param: none
    @return: Creates a text file called "output_avg_shaker_sort.txt"
             Contains the list size and output from time_shaker_sort
    @return: Creates a json file with the raw ouputs to be used in plotting
             the graph of the time taken
    @precondition : none
    @postcondition: a file named output_avg_shaker_sort.txt is modified/created
    @complexity: Worst Case: O(n)
                 where n is the Worst case complexity of shaker_sort
    @complexity: Best Case : O(m)
                 where m is the Best Case complexity of shaker_sort
    """
    json_output = open("json_avg_shaker_sort", "w")
    text_output = open("output_avg_shaker_sort.txt", "w")
    random.seed()
    time_list = []
    power_of_2 = [2**n for n in range(1, 11)]
    for n in power_of_2:
        # generate 100 lists of random numbers of size n
        sum = 0
        for i in range(100):
            print("Processing list ", i, " of length ", n)
            rand_no_list = []
            for _ in range(n):
                rand_no_list.append(random.random())
            sum += time_shaker_sort(rand_no_list)

        avg = sum / 100
        time_list.append(avg)
        print(n, avg, file=text_output)

    json.dump([power_of_2, time_list], json_output)
    text_output.close()
    json_output.close()
