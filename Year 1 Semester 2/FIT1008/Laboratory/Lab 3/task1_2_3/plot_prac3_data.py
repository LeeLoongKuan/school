""""
@description: A module to assist in plotting graphs of the time complexity

@author: Lee Loong Kuan
@since: 09-08-2015
@modified: 10-08-2015
"""

from matplotlib import pyplot
import json
import sys


def plot_data(data_set, fname):
    """
    @description: A function to plot a data set and save it as a file

    @param: a list or tuple of 2 list. ([x-data], [y-data])
    @param: the name to save the plot as, fname
    @return: A file is saved that contains the plot
    @precondition: the data_set is of the correct format
    @postcondition: a file is created
    @complexity: Best and Worst Case: O(n)
    @complexity: where n is the length of the data set
    """
    fig = pyplot.figure()
    fig.clf()
    pyplot.plot(data_set[0], data_set[1], linestyle='solid',
                marker='o', markerfacecolor='red')
    fig.suptitle(fname, fontsize=20)
    pyplot.xlabel('list length, n', fontsize=14)
    pyplot.ylabel('time taken, seconds', fontsize=14)
    fig.savefig(fname)


def plot_all():
    json_in = open("json_sum_items")
    data_set = json.load(json_in)
    plot_data([data_set[0], data_set[1]], "Sum Items")
    json_in.close()
    json_in = open("json_shaker_sort")
    data_set = json.load(json_in)
    plot_data([data_set[0], data_set[1]], "Shaker Sort")
    json_in.close()
    json_in = open("json_avg_shaker_sort")
    data_set = json.load(json_in)
    plot_data([data_set[0], data_set[1]], "Average Shaker Sort")
    json_in.close()
