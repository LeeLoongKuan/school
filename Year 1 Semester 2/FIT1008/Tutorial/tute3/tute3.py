def swap(point1, point2, working_list):
    tmp = working_list[point1]
    working_list[point1] = working_list[point2]
    working_list[point2] = tmp


def shaker_sort(a_list):
    """
    @description: Uses shaker sort to sort a_list
    Shaker sort is a type of bubble sort that
    alternates between moving the largest element and the smallest element
    Note: The function terminates and return the sorted list
          as soon as the list is sorted

    @param: a list of orderable elements, a_list
    @return: A sorted a_list
    @precondition: a_list must only contain orderable elements
    @postcondition: a_list is now sorted
    @complexity: Worst Case: O(n^2) where n is the length of a_list
    @complexity: Best Case : O(n) where n is the length of a_list
    @complecity: Best Case : Occurs for a list thats alreasy sorted
    """
    direc = 1
    is_sorted = False
    ls_pnt = 0
    while not is_sorted:
        is_sorted = True
        for _ in range(len(a_list) - 1):
            ls_pnt += direc
            prev_pnt = ls_pnt - direc
            if (a_list[prev_pnt] * direc) > (a_list[ls_pnt] * direc):
                swap(ls_pnt, prev_pnt, a_list)
                print(a_list)
                is_sorted = False

        direc = direc * -1

"""
@description: Searches for an item in a list and emoves it from the list

@param: a list of elements, a_list; the item to search for and remove, target
@return: none
@precondition: none
@postcondition: an element is removed form a_list
@complexity: Worst Case: O(log(n)) where n is length of a_list
@complexity: Best Case : O(1) when target is at the middle of the list
"""
