(TeX-add-style-hook
 "Loong Kuan_Lee"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "jhwhw"
    "jhwhw10"
    "tikz"
    "tkz-graph"
    "tkz-berge"
    "algorithm"
    "algpseudocode")
   (LaTeX-add-labels
    "fig:Graph G"
    "fig:Circuits"
    "fig:Prim's Algorithm"
    "fig:Paths")))

