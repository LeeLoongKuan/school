def BruteForeceMailProblem(T):
    """ 
    BruteForceMailProblem(T[0...n-1,0...n-1])
    INPUT: A table of times, where T[i,j] indicates the time it takes to travel from location i to location j
    OUTPUT: A list of locations to be visited so that the total distance of the path is the shortest possible using a brute force approach. (Note: only returns the first optimised path it finds)
    ASSUMPTIONS: Stop 0 is the depot, i.e. T[0,j] is the time from the depot to stop j.
    """
    StopsList = []
    i = 0
    # Generate list of all stops execpt depot
    while (i < len(T)-1):
        StopsList.append(i+1)
        i = i + 1
        
    PossiblePaths = [[]]
    PossiblePaths = GetAllPaths(StopsList, PossiblePaths, [])
    
    i = 0
    bestPath = []
    bestTime = 0
    while (i < len(PossiblePaths)):
        tmpTime = GetTime(PossiblePaths[i], T)
        if (tmpTime < bestTime or bestTime == 0):
            bestTime = tmpTime
            bestPath = []
            bestPath.append(PossiblePaths[i])
        elif (bestTime == tmpTime):
            bestPath.append(PossiblePaths[i])
        i = i + 1
        
    i = 0
    while (i < len(bestPath)):
        print(bestPath[i])
        i = i + 1
        
        
def GetAllPaths(StopsLeft, PossiblePaths, CurrentPath):
    """"
    GetAllPaths(StopsLeft[0...n-1], PossiblePaths, CurrentPath)
    INPUT: list of elements, StopsLeft, to permute; List of all possible paths to append upon, PossiblePaths; List of current paths 
    OUTPUT: generates all permutations of elements in StopsLeft and returns a list of lists of the permutations.
    """
    i = 0
    if (len(StopsLeft) < 1):
        PossiblePaths.append(list(CurrentPath))
        return PossiblePaths
    else:
        while (i < len(StopsLeft)):
                CurrentPath.append(StopsLeft[i])
                tmpStops = list(StopsLeft)
                del tmpStops[i]
                PossiblePaths = list(GetAllPaths(tmpStops, PossiblePaths, CurrentPath))
                del CurrentPath[len(CurrentPath) - 1]
                i = i + 1
        return PossiblePaths
    
def GetTime (Path, T):
    """GetTime (Path[0...n-2], T[0...n-1, 0...n-1])
    INPUT: the path we want to calculate the distance of, Path[n-1] ; A table of all the times
    OUTPUT: the total time the path will take. Note that we also include time from depot tot first stop and time from last stop to depot
    """
    TotalTime = 0
    prevStop = 0
    i = 0
    while (i < len(Path)):
        CurrentStop = Path[i]
        TotalTime = TotalTime + T[prevStop][CurrentStop]
        prevStop = CurrentStop
        i = i + 1
        
    TotalTime = TotalTime + T[prevStop][0]
    return TotalTime

T = [[0,8,5,12],[8,0,4,2],[6,9,0,4],[13,3,4,0]]
n = len(T)
StopsLeft = [0,1,2,3]
PossiblePaths = []
CurrentPath = []
