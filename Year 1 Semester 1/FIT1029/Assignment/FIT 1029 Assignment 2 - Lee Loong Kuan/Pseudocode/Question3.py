T = [[0,6,5,10,10,15],[6,0,7,8,7,11],[5,7,0,3,9,4],[10,8,3,0,9,4],[10,7,9,9,0,2],[15,11,4,4,2,0]]
bestSolution = []
partialSolution = []
m = 3
bestDistance = 0
currentDistance = 0

def getState(c):
    if (c == 0 or c == 1):
        return 0
    elif (c == 2 or c == 3):
        return 1
    elif (c == 4 or c == 5):
        return 2
    else: 
        return -1


def isSolution(partialSolution, T, m):
    """
    NAME: isSoultion (partialSolution, T[0...n-1,0...n-1,m])
    INPUT: Partial Solution in the form of a list of city indexes obtained so far, partialSolution; Table of times between stops, T[0...n-1,0...n-1]; number of states, m
    OUTPUT: True if the partial solution is a solution. False otherwise.
    """
    StatesVisited = []
    i = 0
    while (i < len(partialSolution)):
        tmpCity = partialSolution[i]
        tmpState = getState(tmpCity)
        j = 0
        # check for duplicate states and return false if found
        while (j < len(StatesVisited)):
            if (tmpState == StatesVisited[j]):
                return False
            j = j + 1
        StatesVisited.append(tmpState)
        i = i + 1

    # check if the number of states visited is equal to the numbe of states
    if (len(StatesVisited) == m):
        return True
    else:
        return False


def getNextItems(partialSolution, T):
    """
    NAME    : getNextItems(partialSolution, T[0...n-1, 0...n-1)
    INPUT   : a list of previous cities visited, partialSolution; table of times between cities, T[0...n-1,0...n-1]
    OUTPUT  : a list of possible cities to visit next
    """
    n = len(T)
    prevStates = []
    i = 0
    # get all the states visited from the list of previous cities visited
    while (i < len(partialSolution)):
        tmpCity = partialSolution[i]
        tmpState = getState(tmpCity)
        prevStates.append(tmpState)
        i = i + 1

    # iterate over states and check for state and whether a path exists
    NextItems = []
    i = 0
    while (i < n):
        tmpState = getState(i)
        hasPath = True
        newState = True
        j = 0
        # check if state has not been visited
        while (j < len(prevStates)):
            if (prevStates[j] == tmpState):
                newState = False
            j = j + 1
        # check if there's a path between the previous city visted and the city being iterated over
        if (len(partialSolution) > 0):
            if (T[partialSolution[len(partialSolution)-1]][i] == 0):
                hasPath = False
        # if the city being iterated over is in a new state and has a path between the previous city visted
        if (hasPath and newState == True):
            NextItems.append(i)
        i = i + 1
    return NextItems


def getDistance(partialSolution, T):
    """
    NAME    : getDistance(partialSolution, T[0...n-1,0...n-1])
    INPUT   : Cities visited, partialSolution; Time between each city, T[0...n-1,0...n-1]
    OUTPUT  : Total distance of the partial solution
    """
    if (len(partialSolution) < 2):
        return 0
    totalDistance = 0
    prevCity = partialSolution[0]
    i = 1
    while (i < len(partialSolution)):
        currentCity = partialSolution[i]
        totalDistance = totalDistance + T[prevCity][currentCity]
        prevCity = currentCity
        i = i + 1
    return totalDistance    
    

def ProcessSolution(partialSolution, bestSolution, T):
    """
    NAME    : ProcessSolution(partialSolution, bestSolution, T[0...n-1,0...n-1)
    INPUT   : A possible bestPath, partialSolution; The known bestPath, bestSolution; Table of times, T.
    OUTPUT  : The shorter path between partialSolution and bestSolution
    """
    # Return to the first city we started at
    tmpSolution = list(partialSolution)
    tmpSolution.append(tmpSolution[0])
    currentDistance = getDistance(tmpSolution, T)
    bestDistance = getDistance(bestSolution, T)
    if (currentDistance < bestDistance or bestDistance == 0):
        return  tmpSolution 
    else:
        return bestSolution


def StateTSPBacktracker(partialSolution, T, m, bestSolution):
    """
    NAME    : StateTSPBacktracker(partialSolution, T[0...n-1, 0...n-1], m, bestSolution)
    INPUT   : A list of cities visited so far, partialSolution; Table of times between stops, T[0...n-1,0...n-1]; The number of states, m; best solution found so far, bestSolution.
    OUTPUT  : Prints the optimal route that visits one city from each state exactly once
    """
    if (isSolution(partialSolution, T, m) == True):
        return ProcessSolution(partialSolution, bestSolution, T)

    else:
        possibleItems = getNextItems(partialSolution, T)
        k = 0
        while (k < len(possibleItems)):
            partialSolution.append(possibleItems[k])
            bestSolution = list(StateTSPBacktracker(partialSolution, T, m, bestSolution))
            del partialSolution[len(partialSolution) - 1] 
            k = k + 1 
        return bestSolution


def StateTSPBacktracker_part_e(partialSolution, partialDistance, T, m, bestSolution, bestDistance):
    """
    NAME    : StateTSPBacktracker(partialSolution, partialDistance, T[0...n-1, 0...n-1], m, bestSolution)
    INPUT   : A list of cities visited so far, partialSolution; Table of times between stops, T[0...n-1,0...n-1]; The number of states, m; best solution found so far, bestSolution
    OUTPUT  : Prints the optimal route that visits one city from each state exactly once
    """
    if (isSolution(partialSolution, T, m) == True):
        return ProcessSolution(partialSolution, bestSolution, T)

    else:
        tmpCity = 0
        possibleItems = getNextItems(partialSolution, T)
        k = 0
        while (k < len(possibleItems)):  
            # Add or "Visit" next stop
            # Note: length needs to be more than 0 that implies we have already visited a city 
            if (len(partialSolution) > 0):
                tmpCity = partialSolution[len(partialSolution) - 1]
                partialDistance = partialDistance + T[tmpCity][possibleItems[k]]
            partialSolution.append(possibleItems[k])
            # Check if CurrentPath is longer than bestPath
            if (partialDistance < bestDistance or bestDistance == 0):
                bestSolution = list(StateTSPBacktracker_part_e(partialSolution, partialDistance, T, m, bestSolution, bestDistance))
                bestDistance = getDistance(bestSolution, T)
            # Backtrack back to previous city
            # Note: the length need to be more than 1 since distance is 0 when there is only one or less city
            if (len(partialSolution) > 1):
                partialDistance = partialDistance - T[tmpCity][possibleItems[k]]
            del partialSolution[len(partialSolution) - 1]         
            k = k + 1 
        return bestSolution
