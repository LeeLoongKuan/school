def GreedyMailProblem(T):
    """
    GreedyMailProblem(T[0...n-1,0...n-1])
    INPUT: A table of times, where T[i,j] indicates the time it takes to travel from location i to location j.
    OUTPUT: A list of the locations to be visited including the depot using a greedy approach.
    ASSUMPTIONS: Stop 0 is the depot.
    """
    n = len(T)
    CurrentPath = [0]
    i = 0
    # n - 1 because we depot is the start and end point. So no need to consider it.
    while (i < n - 1):
        NextBestStop = GetNextBest(CurrentPath, T)
        CurrentPath.append(NextBestStop)
        i = i + 1
    CurrentPath.append(0)
    print(CurrentPath)


def GetNextBest(CurrentPath, T):
    """
    GetNextBest(StopsVisited[0...k-1], TimeList[0...n-1][0...n-1]
    INPUT: List of previous stops visited, StopsVisited ; List of time from current stop to other stops, TimeList.
    OUTPUT: Index of the stop with the shortest time from current stop.
    """
    n = len(T)
    CurrentStop = CurrentPath[len(CurrentPath)-1]
    bestTime = 0 #Initilize with 0 since we know that is an impossible time
    bestStop = CurrentStop # Again another impossible value
    i = 1 #Start From 1 since we know we start from depot
    while (i < n):
        tmpTime = T[CurrentStop][i]
        if (SequentialSearch(i, CurrentPath) == False):
            if (tmpTime < bestTime or bestTime == 0):
                bestTime = tmpTime
                bestStop = i
        i = i + 1
    return bestStop


def SequentialSearch(Target, List):
    """
    SequentialSearch(Target, List[0...n-1])
    INPUT: A target to search for, Target; A list to search through, List[0...n-1]
    OUTPUT: True if target exists in the list; False otherwise
    """
    i = 0
    while (i < len(List)):
        if (List[i] == Target):
            return True
        i = i + 1
    return False

T = [[0,8,5,12],[8,0,4,2],[6,9,0,4],[13,3,4,0]]
