(TeX-add-style-hook
 "test"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "babel")
   (LaTeX-add-labels
    "sec:introduction")))

