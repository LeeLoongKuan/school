(TeX-add-style-hook
 "Assignment 1"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english")))
   (TeX-run-style-hooks
    "latex2e"
    "jhwhw"
    "jhwhw10"
    "babel"
    "amsthm")))

