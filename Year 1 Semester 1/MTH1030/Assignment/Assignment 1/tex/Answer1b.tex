Let $d$, the fourth non right angled face from slicing a corner off of a cube, be a triangle formed by cutting a parallelogram of vectors $\mathbf{u}$ and $\mathbf{v}$ in half, as illustrated below:
\begin{figure}[!ht]
	\centering
	\input{./img/triangle.tex}
	\caption{triangle $d$ from parallelogram of vectors \textbf{u} and \textbf{v}}
\end{figure}
\\
Therefore, the area of $d$ will be half the length of the cross product of $\mathbf{u}$ and $\mathbf{v}$.\\
\begin{align*}
	d &= \frac{1}{2}|\mathbf{u}\times\mathbf{v}|\\
	&= \frac{1}{2}|(\mathbf{u_1}, \mathbf{u_2}, \mathbf{u_3}) \times (\mathbf{v_1}, \mathbf{v_2}, \mathbf{v_3})|\\
	&= \frac{1}{2}|((\mathbf{u_2}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_2}), -(\mathbf{u_1}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_1}), (\mathbf{u_1}\mathbf{v_2} - \mathbf{u_2}\mathbf{v_1}))|\\
	&= \frac{1}{2}\sqrt[2]{(\mathbf{u_2}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_2})^2 + (-\mathbf{u_1}\mathbf{v_3} + \mathbf{u_3}\mathbf{v_1})^2+(\mathbf{u_1}\mathbf{v_2} - \mathbf{u_2}\mathbf{v_1})^2}\\
	d^2 &= (\frac{1}{2})^2((\mathbf{u_2}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_2})^2 + (-\mathbf{u_1}\mathbf{v_3} + \mathbf{u_3}\mathbf{v_1})^2+(\mathbf{u_1}\mathbf{v_2} - \mathbf{u_2}\mathbf{v_1})^2)\\
\end{align*}
\paragraph{}
	To find the areas $a$, $b$, and $c$, we can imagine that these three triangles, similar to $d$, is made from cutting a parallelogram in half.
\paragraph{}
	Therefore, the areas of $a$, $b$, and $c$ are half the length of the cross product of any two vectors that span two sides of each triangle.
\paragraph{}
	To find the vectors spanning the triangles, we must realize that triangle $a$, $b$, and $c$ are actually orthogonal projections of triangle $d$ on to the sides of the cube where a part is sliced off.
\paragraph{}
	If we take the three sides of the cube $d$ is projected on as the \textit{xz-pane}, the \textit{yz-pane}, and the \textit{xy-pane}, we can see that:
	\begin{itemize}
		\item $a$ is the orthogonal projection of $d$ on the \textit{xz plane}
		\item $b$ is the orthogonal projection of $d$ on the \textit{yz plane}
		\item $c$ is the orthogonal projection of $d$ on the \textit{xy plane}
	\end{itemize}
\newpage
From what we learned in part (a), we can easily find the orthogonal projection of the $\mathbf{u}$ and $\mathbf{v}$ vector on these three faces. The orthogonal projection of $\mathbf{u}$ and $\mathbf{v}$:
\begin{itemize}
	\item On the \textit{xz-plane} are:
	\begin{align*}
	\mathbf{u_a} &= (\mathbf{u_1}, 0, \mathbf{u_3})\\
	\mathbf{v_a} &= (\mathbf{v_1}, 0, \mathbf{v_3})
	\end{align*}
	\item On the \textit{yz-plane} are:
	\begin{align*}
	\mathbf{u_b} = (0, \mathbf{u_2}, \mathbf{u_3})\\
	\mathbf{v_b} = (0, \mathbf{v_2}, \mathbf{v_3})
	\end{align*}
	\item On the \textit{xy-plane} are:
	\begin{align*}
	\mathbf{u_c} &= (\mathbf{u_1}, \mathbf{u_2}, 0)\\
	\mathbf{v_c} &= (\mathbf{v_3}, \mathbf{v_2}, 0)
	\end{align*}
\end{itemize}
With the information above, we can then find the area of $a$, $b$, and $c$ in order to prove that $a^2 + b^2 + c^2 = d^2$.
\begin{proof}
	\begin{align*}
		d^2 &= (\frac{1}{2})^2[(\mathbf{u_2}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_2})^2 + (-\mathbf{u_1}\mathbf{v_3} + \mathbf{u_3}\mathbf{v_1})^2+(\mathbf{u_1}\mathbf{v_2} - \mathbf{u_2}\mathbf{v_1})^2]\\
		\\
		a &= \frac{1}{2}|\mathbf{u_a}\times\mathbf{v_a}|\\
		&= \frac{1}{2}|(\mathbf{u_1}, 0, \mathbf{u_3}) \times (\mathbf{v_1}, 0, \mathbf{v_3})|\\
		&= \frac{1}{2}|((0\mathbf{v_3} - \mathbf{u_3}0), -(\mathbf{u_1}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_1}), (\mathbf{u_1}0 - 0\mathbf{v_1}))|\\
		&= \frac{1}{2}\sqrt[2]{(-\mathbf{u_1}\mathbf{v_3} + \mathbf{u_3}\mathbf{v_1})^2}\\
		a^2 &= (\frac{1}{2})^2(-\mathbf{u_1}\mathbf{v_3} + \mathbf{u_3}\mathbf{v_1})^2\\
		\\
		b &= \frac{1}{2}|\mathbf{u_b}\times\mathbf{v_b}|\\
		&= \frac{1}{2}|(0, \mathbf{u_2}, \mathbf{u_3}) \times (0, \mathbf{v_2}, \mathbf{v_3})|\\
		&= \frac{1}{2}|((\mathbf{u_2}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_2}), -(0\mathbf{v_3} - \mathbf{u_3}0), (0\mathbf{v_2} - \mathbf{u_2}0))|\\
		&= \frac{1}{2}\sqrt[2]{(\mathbf{u_2}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_2})^2}\\
		b^2 &= (\frac{1}{2})^2(\mathbf{u_2}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_2})^2\\
		\\
		c &= \frac{1}{2}|\mathbf{u_c}\times\mathbf{v_c}|\\
		&= \frac{1}{2}|(\mathbf{u_1}, \mathbf{u_2}, 0) \times (\mathbf{v_1}, \mathbf{v_2}, 0)|\\
		&= \frac{1}{2}|((\mathbf{u_2}0 - 0\mathbf{v_2}), -(\mathbf{u_1}0 - 0\mathbf{v_1}), (\mathbf{u_1}\mathbf{v_2} - \mathbf{u_2}\mathbf{v_1}))|\\
		&= \frac{1}{2}\sqrt[2]{(\mathbf{u_1}\mathbf{v_2} - \mathbf{u_2}\mathbf{v_1})^2}\\
		c^2 &= (\frac{1}{2})^2(\mathbf{u_1}\mathbf{v_2} - \mathbf{u_2}\mathbf{v_1})^2\\
		\\
		a^2 + b^2 + c^2 &= (\frac{1}{2})^2(-\mathbf{u_1}\mathbf{v_3} + \mathbf{u_3}\mathbf{v_1})^2 + (\frac{1}{2})^2(\mathbf{u_2}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_2})^2 + (\frac{1}{2})^2(\mathbf{u_1}\mathbf{v_2} - \mathbf{u_2}\mathbf{v_1})^2\\
		&= (\frac{1}{2})^2[(\mathbf{u_2}\mathbf{v_3} - \mathbf{u_3}\mathbf{v_2})^2 + (-\mathbf{u_1}\mathbf{v_3} + \mathbf{u_3}\mathbf{v_1})^2 + (\mathbf{u_1}\mathbf{v_2} - \mathbf{u_2}\mathbf{v_1})^2]\\
		&= d^2
	\end{align*}
\end{proof}