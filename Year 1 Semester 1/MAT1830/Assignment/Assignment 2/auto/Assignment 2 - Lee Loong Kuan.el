(TeX-add-style-hook
 "Assignment 2 - Lee Loong Kuan"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english")))
   (TeX-run-style-hooks
    "latex2e"
    "jhwhw"
    "jhwhw10"
    "babel"
    "amsmath"
    "amssymb")))

