
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

\documentclass{article}
\usepackage{amsmath}

\title{Matrix Cambridge A-Levels}
\author{Lee Loong Kuan}

\begin{document}
\pagenumbering{gobble}
\maketitle
\newpage
\pagenumbering{arabic}

\section{System of Linear Equations}
\paragraph{Introduction}:\\ 
One very common use of matrices is to represent a system of linear equations. The matrix derived from the coefficients and constant terms of a system of linear equations is called the augmented matrix of the system.
\newline
The matrix containing only the coefficients of the system is called the coefficient matrix of the system. The following is an example:
\newline
\begin{align*}
x - 4y + 3z &= 5 \\
-x + 3y - z &= 3 \\
2x - 4z &= 6 \\
system
\end{align*}

\begin{align*}
\left[
\begin{matrix}
1 & -4 & 3 & 5\\
-1 & 3 & -1 & -3\\
2 & 0 & -4 & 6\\
\end{matrix}
\right] \\
Augmented Matrix
\end{align*}

\begin{align*}
\left[
\begin{matrix}
1 & -4 & 3\\
-1 & 3 & -1\\
2 & 0 & -4\\
\end{matrix}
\right] \\
Coefficient Matrix
\end{align*}

\section{Elementary Row Options}
\paragraph{Introduction}:\\ 
An elementary row operation on an augmented matrix produces a new augmented matrix corresponding to a new (but equivalent) system of linear equations. Two matrices are said to be row-equivalent if one can be obtained from the other by a finite sequence of elementary row operations. 

\paragraph{Elementary Row Operations}:
\begin{itemize}
\item Interchange two rows
\item Multiply a row by a non-zero constant
\item Add a multiple of a row to another row
\end{itemize}


\paragraph{Definition}:\\
A matrix in row-echelon form has the following properties:
\begin{itemize}
\item All rows consisting entirely of zeros occur at the bottom of the matrix.
\item For each row that does not consist entirely of zeros, the first non-zero entry is 1 (called a leading 1).
\item For to successive (non-zero) rows, the leading 1 in the higher row further to the left than the leading 1 in the lower row.
\end{itemize}

\newpage

\section{Using Elementary Row Operations to Solve a System (Gaussian Elimination)}
\paragraph{Example 1}:
\begin{align*}
\newline
\begin{bmatrix}
  1 & -2 & 3 & 9\\
  -1 & 3 & 0 & -4\\
  2 & -5 & 5 & 17
\end{bmatrix}
\xrightarrow{R_2  \rightarrow  R_2 + R_1}&
\begin{bmatrix}
  1 & -2 & 3 & 9\\
  0 & 1 & 3 & 5\\
  2 & -5 & 5 & 17
\end{bmatrix}\\
\newline
\xrightarrow{R_3  \rightarrow  R_3 - 2R_1}&
\begin{bmatrix}
  1 & -2 & 3 & 9\\
  0 & 1 & 3 & 5\\
  0 & -1 & -1 & -1
\end{bmatrix}\\
\newline
\xrightarrow{R_3 \rightarrow R_3 + R_2}&
\begin{bmatrix}
  1 & -2 & 3 & 9\\
  0 & 1 & 3 & 5\\
  0 & 0 & 2 & 4
\end{bmatrix}\\
\newline
\xrightarrow{R_3 \rightarrow \frac{1}{2}R_3}&
\begin{bmatrix}
  1 & -2 & 3 & 9\\
  0 & 1 & 3 & 5\\
  0 & 0 & 1 & 2
\end{bmatrix}\\
\end{align*}

\begin{align*}
Therefore: x - 2y + 3z &= 9\\
y + 3z &= 5\\
z &= 2\\ \\
Hence: x = 1 , y = -1 , and z &= 2
\end{align*}

\paragraph{Exercise 1: Solve the System}
\begin{align*}
x_2 + x_3 - 2x_4 &= -3\\
x_1 + 2x_2 - x_3 &= 2\\
2x_1 + 4x_2 + x_3 -3x_4 &= -2\\
x_1 - 4x_2 - 7x_3 - x_4 &= -19\\ \\
(Answer: x_1 = -1 , x_2 = 2 , x_3 &= 1 , x_4 = 3)
\end{align*}

\paragraph{Exercise 2: Solve the System}
\begin{align*}
x_1 - x_2 + 2x_3 &= 4\\
x_1 + x_3 &= 6\\
2x_1 - 3x_2 + 5x_3 &= 4\\
3x_1 + 2x_2 - x_3 &= 1\\
(A \ system \ with \ no \ solu&tion)
\end{align*}

\newpage

\section{Gauss-Jordan Elimination}
\paragraph{Introduction}:\\
With Gaussian Elimination, you apply elementary row operations to a matrix to obtain a (row-equivalent) row-echelon form.\\
A second method of elimination, called Gauss-Jordan Elimination, continues the reduction process until a reduced row-echelon form is obtained.

\paragraph{Remark}:\\
A matrix in row-echelon form is in reduced row-echelon form if every column that has a leading 1 has zeros in every position above and below its leading 1.\\

\paragraph{Example 2 (from Example 1)}:
\begin{align*}
\begin{bmatrix}
1 & -2 & 3 & 9\\
0 & 1 & 3 & 5\\
0 & 0 & 1 & 2
\end{bmatrix}
\xrightarrow{R_1 \rightarrow R_1 + 2R_2}&
\begin{bmatrix}
1 & 0 & 9 & 19\\
0 & 1 & 3 & 5\\
0 & 0 & 1 & 2
\end{bmatrix}\\
\newline
\xrightarrow{R_1 \rightarrow R_1 - 9R_3}&
\begin{bmatrix}
1 & 0 & 0 & 1\\
0 & 1 & 3 & 5\\
0 & 0 & 1 & 2
\end{bmatrix}\\
\newline
\xrightarrow{R_2 \rightarrow R_2 - 3R_3}&
\begin{bmatrix}
1 & 0 & 0 & 1\\
0 & 1 & 0 & -1\\
0 & 0 & 1 & 2
\end{bmatrix}\\ \\
(x=1,y=-1,z&=2)
\end{align*}

\paragraph{Example 3 (A system with an Infinite Number of Solutions)}
\begin{align*}
\begin{bmatrix}
2 & 4 & -2 & 0\\
3 & 5 & 0 & 1
\end{bmatrix}
\xrightarrow
[{R_2 \rightarrow\frac{1}{3}R_2}]
{{R_1 \rightarrow\frac{1}{2}R_1}}&
\begin{bmatrix}
1 & 2 & -1 & 0\\
1 & \frac{5}{3} & 0 & \frac{1}{3}
\end{bmatrix}\\
\newline
\xrightarrow{R_2 \rightarrow R_2 - R_1}&
\begin{bmatrix}
1 & 2 & -1 & 0\\
0 & \frac{-1}{3} & 1 & \frac{1}{3}
\end{bmatrix}\\
\newline
\xrightarrow{R_2 \rightarrow - 3R_3}&
\begin{bmatrix}
1 & 2 & -1 & 0\\
0 & 1 & -3 & -1
\end{bmatrix}\\
\newline
\xrightarrow{R_1 \rightarrow R_1 - 2R_2}&
\begin{bmatrix}
1 & 0 & 5 & 2\\
0 & 1 & -3 & -1
\end{bmatrix}\\ \\
Therefore : x_1 + 5x_3 = 2&\\
x_2 - 3x_3 = -1&
\end{align*}

Using parameter $t$ to represent the non-leading variable $x_3$, we have:\\
$x_1 = 2 - 5t$, $x_2 = -1 + 3t$, $x_3 = t$, where $t$ is any real number.

\paragraph{Exercise 3: Solve the System of Linear Equations}
\begin{align*}
x_1 - x_2 + 3x_3 &= 0\\
2x_1 + x_2 + 3x_3 &= 0
\end{align*}

\paragraph{Remark}:\\
Every homogeneous system of linear equations is consistent. Moreover, if the system has fewer equations than variables, then it must have an infinite number of solutions. 

\newpage

\section{Linear Independence}
\paragraph{Definition}:\\
Let $\vec{v}_1$, $\vec{v}_2$, \ldots, $\vec{v}_n$ be $n$ vectors in a vector space.\\
Let $S = {\vec{v}_1, \vec{v}_2, \ldots, \vec{v}_n}$. Then
\begin{itemize}
\item 
S is called a linearly independent set or $\vec{v}_1$, $\vec{v}_2$, \ldots, $\vec{v}_n$ are called linear independent if the vector equations
\[
\lambda_1\vec{v}_1 + \lambda_2\vec{v}_2 + \ldots + \lambda_n\vec{v}_n = \vec{0} \quad \quad
(\lambda_i \in F, i = 1, 2, \ldots)
\]
has only the trivial solution, $\lambda_1 = 0, \lambda_2 = 0, \ldots, \lambda_n = 0$

\item
S is called a linearly dependent set or $\vec{v}_1$, $\vec{v}_2$, \ldots, $\vec{v}_n$ are called linearly dependent if there exist scalars $\lambda_1, \lambda_2, \ldots, \lambda_n$ not all zero\\
(i.e. exists some $\lambda_i \neq 0$; $i = 1, 2, \ldots, n$)\\
such that $\lambda_1\vec{v}_1 + \lambda_2\vec{v}_2 + \cdots + \lambda_n\vec{v}_n = \vec{0}$
\end{itemize}

\paragraph{Example 1}:\\
Let $S = {\vec{v}_1, \vec{v}_2, \vec{3}_3}$ where $\vec{v}_1=(2, -1, 0, 3), \vec{v}_2=(1, 2, 5, -1), \vec{v}_3=(7, -1, 5, 8)$. Then $S$ is a linearly dependent set, since there exist $\lambda_1 = 3, \lambda_2 = 1, \lambda_3 = -1$ (not all zero) such that $3\vec{v}_1 + 2\vec{v}_2 - \vec{v}_3$


\end{document}

